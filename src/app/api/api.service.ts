import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError, timer, pipe } from 'rxjs';
import { switchMap, map, mergeMap } from 'rxjs/operators';
import { Callback, CognitoUtil, LoggedInCallback } from '../service/cognito.service';
import { UserLoginService } from '../service/user-login.service';
import { TokenService } from '../shared/token.service';
import { Stuff, AlertsResponse, ChartDataResponse, ActionsResponse } from './api.model';
import { environment } from '../../environments/environment';
import * as moment from 'moment';

const intervalTime = 120000;

@Injectable()
export class ApiService implements LoggedInCallback {

  public stuff: Stuff = new Stuff();
  public updatedEndDate: string = '';
  private metricIntervalEndDate: Object = {};
  private metricIntervalStartDate: Object = {};

  constructor(
    public router: Router,
    private http: HttpClient,
    public userService: UserLoginService,
    public cognitoUtil: CognitoUtil,
    private token: TokenService
  ) {
    // this.userService.isAuthenticated(this);
    this.cognitoUtil.getAccessToken(new AccessTokenCallback(this));
    this.cognitoUtil.getIdToken(new IdTokenCallback(this));

  }

  isLoggedIn(message: string, isLoggedIn: boolean) {
    if (!isLoggedIn) {
      console.log('apiService not login');
      this.router.navigate(['/home/login']);
    } else {
      console.log('apiService login successful');
      this.cognitoUtil.getAccessToken(new AccessTokenCallback(this));
      this.cognitoUtil.getIdToken(new IdTokenCallback(this));
    }
  }


  private createHttpHeader() {
    console.log('API Token ' + this.token.toString());
    return new HttpHeaders({
      'Authorization': 'Bearer ' + this.token.toString()
    });
  }

  public getAlertData(site: string): Observable<AlertsResponse> {
    // return this.http.get<AlertsResponse>('/pages/mhs/mhs-web-ui/assets/json/mhs_alert_data.json');//.map();
    // return this.http.get<AlertsResponse>('/assets/json/mhs_alerts.json');//.map();
    // return this.http.get<AlertsResponse>('./assets/json/mhs_alerts.json');
    // return this.http.get<AlertsResponse>('http://10.0.2.10/alerts/getAlerts');
    const url = environment.apiURL + '/location?' + 'site=' + encodeURIComponent(site);
    return this.http.get<AlertsResponse>(url, { headers: this.createHttpHeader() });
    // return this.http.get<AlertsResponse>('./assets/json/mhs_alerts.json');
  }

  public getChartData(
    site: string,
    sensor: string,
    metric: string,
    timestamp_start: Date,
    timestamp_end: Date
  ): Observable<ChartDataResponse> {
    const url = environment.apiURL + '/data?'
      + 'site=' + site
      + '&sensor=' + encodeURIComponent(sensor)
      + '&metric=' + encodeURIComponent(metric)
      + '&time1=' + timestamp_start.toISOString()
      + '&time2=' + timestamp_end.toISOString()
      + '&convert2metric=0';

    return this.http.get<ChartDataResponse>(url, { headers: this.createHttpHeader() });
  }

  public cacheUpdate(params) {
    const url = `${environment.apiURL}/${params['type']}?site=${params['site']}&cache=update`;
    return this.http.get(url, { headers: this.createHttpHeader() });
  }

  public getIntervalChartData(
    site: string,
    sensor: string,
    metric: string,
    timestamp_start: Date,
    timestamp_end: Date
  ): Observable<ChartDataResponse> {
    this.metricIntervalStartDate[metric] = timestamp_start.toISOString();
    this.metricIntervalEndDate[metric] = timestamp_end.toISOString();

    return timer(0, intervalTime)
      .pipe(
        switchMap(() => {
          const url = environment.apiURL + '/data?'
            + 'site=' + site
            + '&sensor=' + encodeURIComponent(sensor)
            + '&metric=' + encodeURIComponent(metric)
            + '&time1=' + this.metricIntervalStartDate[metric]
            + '&time2=' + this.metricIntervalEndDate[metric]
            + '&convert2metric=0';

          this.updatedEndDate = this.metricIntervalEndDate[metric];
          this.metricIntervalStartDate[metric] = moment(this.metricIntervalEndDate[metric]).toISOString();
          this.metricIntervalEndDate[metric] = moment(this.metricIntervalEndDate[metric]).add(intervalTime, 'ms').toISOString();

          return this.http.get<ChartDataResponse>(url, { headers: this.createHttpHeader() });
        })
      );
  }

  public getSensorChartData(sensorRequest): Observable<ChartDataResponse> {
    const url = environment.apiURL + '/msdata?'
      + 'site=' + sensorRequest.site
      + '&sensor=' + encodeURIComponent(sensorRequest.sensor)
      + '&metric=' + encodeURIComponent(sensorRequest.metric)
      + '&time1=' + sensorRequest.timestamp_start.toISOString()
      + '&time2=' + sensorRequest.timestamp_end.toISOString()
      + '&convert2metric=0';
    const sensor = sensorRequest.sensor;

    return this.http.get<ChartDataResponse>(url, { headers: this.createHttpHeader() })
    .pipe(
      map(result => {
        result['sensor'] = sensor;
        return result;
      })
    );
  }

  public getActionsData(): Observable<ActionsResponse> {
    return this.http.get<ActionsResponse>('./assets/json/mhs_actions_data.json');
  }


  // public getAlertData():Observable<Object> {
  //   // return this.http.get('/pages/dipen-pradhan/mhs-front-end/assets/json/mhs_alert_data.json');//.map();
  //   return this.http.get('/assets/json/mhs_alert_data.json');
  // }

  // public getAlertData():Observable<Object> {
  //   // return this.http.get('/pages/dipen-pradhan/mhs-front-end/assets/json/mhs_alert_data.json');//.map();
  //   return this.http.get('/assets/json/mhs_alert_data.json');
  // }

  public getUserDetails() {
    const userDetailsUrl = 'https://mbu1pyaok9.execute-api.us-east-1.amazonaws.com/prod/groups?'
    + 'token=' + this.token.toString();

    return this.http.get(userDetailsUrl);
  }
}

export class AccessTokenCallback implements Callback {
  constructor(public apiService: ApiService) { }

  callback() { }

  callbackWithParam(result) {
    this.apiService.stuff.accessToken = result;
  }
}

export class IdTokenCallback implements Callback {
  constructor(public apiService: ApiService) { }

  callback() { }

  callbackWithParam(result) {
    this.apiService.stuff.idToken = result;
  }
}
