import { Callback } from '../service/cognito.service';

export interface Alert {
  trunkline: number;
  location: string;
  site: string;
  conveyor: string;
  description: string;
  equipment: string;
  sensor: string;
}

export interface ChartData {
  read_time: string;
  value: string;
}

export interface ChartDataSensor {
  datetime: string;
  conveyor: string;
  part: string;
  location: string;
  TEMP_F: number;
  Z_RMS_IPS: number;
  X_RMS_V_IPS: number;
}

export interface ChartDataConveyor {
  datetime: string;
  conveyor: string;
  speed: number;
  amps: number;
  rate: number;
}

export interface AlertsResponse {
  data: Alert[];
}

export interface ChartDataResponse {
  data: ChartData[];
}

export interface ChartDataSensorResponse {
  results: ChartDataSensor[];
}

export interface ChartDataConveyorResponse {
  results: ChartDataConveyor[];
}

export interface ActionsResponse {
  results: any[];
}

export class Stuff {
  public accessToken: string;
  public idToken: string;
}

