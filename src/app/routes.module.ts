import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent, AboutComponent } from './public/home.component';
import { LoginComponent } from './public/auth/login/login.component';
import { RegistrationConfirmationComponent, LogoutComponent } from './public/auth/confirm/confirmRegistration.component';
import { ResendCodeComponent } from './public/auth/resend/resendCode.component';
import { ForgotPassword2Component, ForgotPasswordStep1Component } from './public/auth/forgot/forgotPassword.component';
import { NewPasswordComponent } from './public/auth/newpassword/newpassword.component';
import { JwtComponent } from './secure/jwttokens/jwt.component';
import { UseractivityComponent } from './secure/useractivity/useractivity.component';
import { KnowledgePageComponent } from './knowledge-page/knowledge-page.component';
import { SearchResultComponent } from './search-result/search-result.component';
import { FacilityOverviewComponent } from './facility-overview/facility-overview.component';
import { GuardRoute } from './core/guard.route';
import { AlertDetailPageComponent } from './alert-details/alert-detail-page/alert-detail-page.component';
import { AlertPageComponent } from './alert-page/alert-page.component';
import { AlertConveyorComponent } from './alert-page/alert-conveyor/alert-conveyor.component';
import { UsersComponent } from './admin/users/users.component';
import { MyProfileComponent } from './admin/users/my-profile/my-profile.component';
import { SesComponent } from './admin/users/ses/ses.component';
import { SnsComponent } from './admin/users/sns/sns.component';
import { EditUsersComponent } from './admin/users/edit-users/edit-users.component';
import { AdminComponent } from './admin/admin.component';
import { SensorsComponent } from './admin/sensors/sensors.component';
import { CacheUpdateGuardService } from './shared/cache-update-guard.service';
import { RegisterComponent } from './admin/users/register/register.component';


const homeRoutes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: HomeComponent,
    children: [
      { path: 'about', component: AboutComponent },
      { path: 'login', component: LoginComponent },
      { path: 'register', component: RegisterComponent },
      { path: 'confirmRegistration/:username', component: RegistrationConfirmationComponent },
      { path: 'resendCode', component: ResendCodeComponent },
      { path: 'forgotPassword/:email', component: ForgotPassword2Component },
      { path: 'forgotPassword', component: ForgotPasswordStep1Component },
      { path: 'newPassword', component: NewPasswordComponent },
      { path: '', component: AlertPageComponent }
    ]
  },
];

const secureHomeRoutes: Routes = [
  {
    path: '',
    redirectTo: '/alerts',
    pathMatch: 'full'
  },
  {
    path: 'admin',
    component: AdminComponent,
    children: [
      { path: 'logout', component: LogoutComponent },
      { path: 'jwttokens', component: JwtComponent },
      { path: 'register', component: RegisterComponent },
      { path: 'myprofile', component: MyProfileComponent },
      { path: 'sensors', component: SensorsComponent, canDeactivate: [CacheUpdateGuardService]},
      { path: 'ses', component: SesComponent },
      { path: 'sns', component: SnsComponent },
      { path: 'myprofile/:username', component: EditUsersComponent },
      { path: 'useractivity', component: UseractivityComponent },
      { path: '', component: AlertPageComponent }
    ],
    canActivate: [GuardRoute]
  }
];

const routes: Routes = [
  {
    path: '',
    children: [
      ...homeRoutes,
      ...secureHomeRoutes,
      { path: 'alerts', component: AlertPageComponent },
      { path: 'alerts/:conveyor', component: AlertConveyorComponent },
      { path: 'alerts/details/:site/:id', component: AlertDetailPageComponent },
      { path: 'knowledge', component: KnowledgePageComponent },
      { path: 'search', component: SearchResultComponent },
      { path: 'overview', component: FacilityOverviewComponent },
      { path: '**', redirectTo: '/alerts', pathMatch: 'full' }
    ]
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class RoutesModule { }
