import { TestBed, inject } from '@angular/core/testing';

import { CacheUpdateGuardService } from './cache-update-guard.service';

describe('CacheUpdateGuardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CacheUpdateGuardService]
    });
  });

  it('should be created', inject([CacheUpdateGuardService], (service: CacheUpdateGuardService) => {
    expect(service).toBeTruthy();
  }));
});
