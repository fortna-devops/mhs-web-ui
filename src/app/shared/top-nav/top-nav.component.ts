import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { TopNavService } from './top-nav.service';
import { Router, NavigationStart } from '@angular/router';
import { UserLoginService } from '../../service/user-login.service';
import { CognitoUtil } from '../../service/cognito.service';
import { AuthService } from '../../core/auth.service';

@Component({
  selector: 'app-top-nav',
  templateUrl: './top-nav.component.html',
  styleUrls: ['./top-nav.component.scss']
})
export class TopNavComponent implements OnInit {
  private title: string;
  public siteList: Array<string> = [];
  public siteId: string;
  public activeSiteName: string;
  public visible = true;
  public showBackButton = false;
  public isAdmin = false;
  public tabs = [{
    name: 'Alerts',
    routerLink: '/alerts'
  }];


  constructor (
    public topNavService: TopNavService,
    private location: Location,
    private router: Router,
    public userService: UserLoginService,
    public cognitoUtil: CognitoUtil,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.isAdmin = this.authService.isUserAdmin();

    this.router.events.subscribe(ev => {
      if (ev instanceof NavigationStart) {
        if (ev.url.indexOf('/alerts/') !== -1 || ev.url.indexOf('/alerts/details/') !== -1) {
          this.showBackButton = true;
        } else {
          this.showBackButton = false;
        }
      }
    });

    this.activeSiteName = this.topNavService.getActiveSiteName();
    this.siteList = this.topNavService.getAllSites();

    this.topNavService.getTitle().subscribe(title => {
      this.title = title;
    });
    this.topNavService.getVisible().subscribe(visible => {
      this.visible = visible;
    });

    // if (this.authService.isUserAdmin()) {
    //   this.tabs.push({
    //     name: 'List Users' ,
    //     routerLink: '/securehome/myprofile'
    //   });
    // }
  }

  logout() {
    this.authService.removeCookies();
    window.location.href = '.';
  }

  isLoggedIn(message: string, isLoggedIn: boolean) {
    if (!isLoggedIn) {
      this.visible = false;
      this.router.navigate(['/home/login']);
    } else {
      this.visible = true;
      this.router.navigate(['/alerts']);
    }
  }

  performSearch(value) {
    this.router.navigateByUrl('/search');
  }

  getTitle() {
    this.topNavService.getTitle();
  }

  setSiteId(siteId) {
    const newActiveSiteName = this.siteList
        .filter(item => item['site_id'] === siteId)
        .map(site => site['name'])
        .find(site => site);

    if (newActiveSiteName === this.activeSiteName) {
      return;
    }

    this.topNavService.setSiteId(siteId);
    window.location.href = '.';
  }

  goBack() {
    this.location.back();
  }
}
