import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { TokenService } from '../token.service';

@Injectable()
export class TopNavService {
  private title = new Subject<any>();
  public visible = new Subject<any>();

  constructor(
    private tokenService: TokenService
  ) { }

  public hide(): void {
    this.visible.next(false);
  }

  public show(): void {
    this.visible.next(true);
  }

  public getVisible(): Observable<any> {
    return this.visible.asObservable();
  }

  public setTitle(title: string): void {
    this.title.next(title);
  }

  public getTitle(): Observable<any> {
    return this.title.asObservable();
  }

  public setSiteId(siteId): void {
    this.tokenService.setSiteId(siteId);
  }

  public getSiteId(): string {
    return this.tokenService.getSiteId();
  }

  public getSiteListId(): Array<string> {
    let siteListId: Array<string> = [];
    const cookiesSiteList = this.tokenService.getSiteList();

    if (cookiesSiteList) {
      siteListId = JSON.parse(cookiesSiteList).map((item) => item.site_id);
    }

    return siteListId;
  }

  public getActiveSiteName(): string {
    let siteName = '';
    const cookiesSiteList = this.tokenService.getSiteList();
    const siteId = this.tokenService.getSiteId();

    if (cookiesSiteList && siteId) {
      siteName = JSON.parse(cookiesSiteList)
        .filter(item => item.site_id === siteId)
        .map(site => site.name)
        .find(site => site);
    }

    return siteName;
  }

  public getAllSites(): Array<string> {
    let siteListId: Array<string> = [];
    const cookiesSiteList = this.tokenService.getSiteList();
    if (cookiesSiteList) {
      siteListId = JSON.parse(cookiesSiteList);
    }

    return siteListId;
  }
}
