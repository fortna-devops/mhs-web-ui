import { Injectable } from '@angular/core';
import { CookieStorage } from 'cookie-storage';

@Injectable()
export class TokenService {
  private userGroups: Array<string> = [];
  private cookieStorage = new CookieStorage();

  public toString(): string {
    return this.cookieStorage.getItem('token');
  }

  public getSiteId(): string {
    return this.cookieStorage.getItem('site');
  }

  public setSiteId(siteId): void {
    this.cookieStorage.setItem('site', siteId);
  }

  public getSiteList(): string {
    return this.cookieStorage.getItem('sites');
  }

  private decodeToken(): any {
    const token = this.cookieStorage.getItem('token');
    if (token === null || token === '') {
      return {};
    }

    const parts = token.split('.');
    if (parts.length !== 3) {
      return {};
    }
    let json = parts[1].replace(/-/g, '+').replace(/_/g, '/');
    switch (json.length % 4) {
      case 0:
          break;
      case 2:
          json += '==';
          break;
      case 3:
          json += '=';
          break;
      default:
          return {};
      }
    return JSON.parse(decodeURIComponent((<any>window).escape(window.atob(json))));
  }

  public setUserGroups() {
    this.userGroups = this.decodeToken()['cognito:groups'];
  }

  public getUserGroups() {
    return this.userGroups.length > 0 ? this.userGroups : this.decodeToken()['cognito:groups'];
  }

  public getAccessKey() {
    return this.cookieStorage.getItem('access_key');
  }

  public getSecretAccessKey() {
    return this.cookieStorage.getItem('secret_access_key');
  }

  public removeCookies() {
    this.cookieStorage.removeItem('token');
    this.cookieStorage.removeItem('access_key');
    this.cookieStorage.removeItem('secret_access_key');
    this.cookieStorage.removeItem('site');
    this.cookieStorage.removeItem('sites');
  }
}
