import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { MainMaterialModule } from '../main-material.module';
import { HelperService } from './helper.service';
import { ModalDialogComponent } from './modal-dialog/modal-dialog.component';
import { TopNavService } from './top-nav/top-nav.service';
import { TopNavComponent } from './top-nav/top-nav.component';
import { FormsModule } from '@angular/forms';
import { TokenService } from './token.service';
import { TwilioService } from './twilio.service';
import { ActiveFiltersService } from './active-filters.service';
import { CacheUpdateGuardService } from './cache-update-guard.service';


@NgModule({
  imports: [
    CommonModule,
    MainMaterialModule,
    RouterModule,
    FormsModule
  ],
  declarations: [
    ConfirmDialogComponent,
    ModalDialogComponent,
    TopNavComponent
  ],
  providers: [
    HelperService,
    TopNavService,
    TokenService,
    TwilioService,
    ActiveFiltersService,
    CacheUpdateGuardService
  ],
  exports: [
    ModalDialogComponent,
    TopNavComponent
  ],
  entryComponents: [
    ConfirmDialogComponent
  ]
})
export class SharedModule { }
