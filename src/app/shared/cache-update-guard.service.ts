import { Injectable } from '@angular/core';
import { CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { SensorsComponent } from '../admin/sensors/sensors.component';
import { ApiService } from '../api/api.service';
import { TopNavService } from './top-nav/top-nav.service';
import { Observable, Observer } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CacheUpdateGuardService implements CanDeactivate<SensorsComponent> {
  constructor(
    private apiService: ApiService,
    private topNavService: TopNavService
  ) { }

  canDeactivate(
    sensorsComponent: SensorsComponent,
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot) {
    const that = this;
    const params = {
      type: 'location',
      site: this.topNavService.getSiteId()
    };

    return Observable.create((observer: Observer<boolean>) => {
      sensorsComponent.isLoading = true;
      this.apiService.cacheUpdate(params)
        .subscribe(result => {
          sensorsComponent.isLoading = false;
          observer.next(true);
          observer.complete();
        });
    });
  }
}
