import { Injectable } from '@angular/core';

@Injectable()
export class HelperService {
  sortByProperty(prop) {
    return (obj1, obj2) => {
      if (obj1[prop] > obj2[prop]) {
        return 1;
      }
      if (obj1[prop] < obj2[prop]) {
        return -1;
      }
      return 0;
    };
  }

  sortByTwoProperty(prop1, prop2) {
    return (obj1, obj2) => {
      return obj1[prop1] - obj2[prop1] || obj1[prop2] - obj2[prop2];
    };
  }
}
