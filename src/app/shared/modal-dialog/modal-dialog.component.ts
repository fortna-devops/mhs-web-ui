import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-modal-dialog',
  templateUrl: './modal-dialog.component.html',
  styleUrls: ['./modal-dialog.component.scss']
})
export class ModalDialogComponent implements OnInit {

  @Input()
  public show: boolean = false;

  @Output()
  public showChange = new EventEmitter<boolean>();

  @Input()
  public message: string;

  constructor() { }

  ngOnInit() {
  }


  public toggleModal(): void {

    this.show = !this.show;
    this.showChange.emit(this.show);
  }
}
