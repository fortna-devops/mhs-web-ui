import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs';


@Injectable()
export class TwilioService {

  private twilioUrl: string = 'https://api.twilio.com/2010-04-01/Accounts/AC7c6795701def86b40209b39d8b1e988a/Messages.json';

  constructor(
    private http: HttpClient
  ) { }

  public sendSMS(alert: any): Observable<any> {
    const body = new URLSearchParams();
    body.set('To', '+15025521774');
    body.set('From', '+16467605215');
    body.set('Body', alert.asset + ' ' + alert.description);

    return this.http.post(this.twilioUrl, body.toString(), {
      headers: new HttpHeaders()
      .set('Authorization', 'Basic QUM3YzY3OTU3MDFkZWY4NmI0MDIwOWIzOWQ4YjFlOTg4YToxZDNjMWY2MGMxOWM0NjlmNmNhYzJlZjUyMjQ2ZjZmMQ==')
      .set('Content-Type', 'application/x-www-form-urlencoded'),
    });

  }

  public sendSMS2(alert: any): Observable<any> {
    const body = new URLSearchParams();
    body.set('To', '+12129204182');
    body.set('From', '+16467605215');
    body.set('Body', alert.asset + ' ' + alert.description);

    return this.http.post(this.twilioUrl, body.toString(), {
      headers: new HttpHeaders()
      .set('Authorization', 'Basic QUM3YzY3OTU3MDFkZWY4NmI0MDIwOWIzOWQ4YjFlOTg4YToxZDNjMWY2MGMxOWM0NjlmNmNhYzJlZjUyMjQ2ZjZmMQ==')
      .set('Content-Type', 'application/x-www-form-urlencoded'),
    });
  }
}
