import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { KnowledgePageComponent } from './knowledge-page/knowledge-page.component';
import { ModuleWithProviders } from '@angular/core';
import { AboutComponent, HomeComponent, HomeLandingComponent } from './public/home.component';
import { JwtComponent } from './secure/jwttokens/jwt.component';
import { UseractivityComponent } from './secure/useractivity/useractivity.component';
import { LoginComponent } from './public/auth/login/login.component';
import { RegisterComponent } from './public/auth/register/registration.component';
import { ForgotPassword2Component, ForgotPasswordStep1Component } from './public/auth/forgot/forgotPassword.component';
import { LogoutComponent, RegistrationConfirmationComponent } from './public/auth/confirm/confirmRegistration.component';
import { ResendCodeComponent } from './public/auth/resend/resendCode.component';
import { NewPasswordComponent } from './public/auth/newpassword/newpassword.component';
import { SearchResultComponent } from './search-result/search-result.component';
import { ApiService } from './api/api.service';
import { AlertPageService } from './alert-page/alert-page.service';
import { FacilityOverviewComponent } from './facility-overview/facility-overview.component';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { UserRegistrationService } from './service/user-registration.service';
import { UserParametersService } from './service/user-parameters.service';
import { UserLoginService } from './service/user-login.service';
import { CognitoUtil } from './service/cognito.service';
import { AwsUtil } from './service/aws.service';
import { DynamoDBService } from './service/ddb.service';
import { MFAComponent } from './public/auth/mfa/mfa.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RoutesModule } from './routes.module';
import { MainMaterialModule } from './main-material.module';
import { SharedModule } from './shared/shared.module';
import { CoreModule } from './core/core.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToasterModule } from 'angular2-toaster';
import { AlertDetailsModule } from './alert-details/alert-details.module';
import { AlertPageModule } from './alert-page/alert-page.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import 'd3';
import 'nvd3';
import { AdminModule } from './admin/admin.module';


@NgModule({
  declarations: [
    AppComponent,
    KnowledgePageComponent,
    SearchResultComponent,
    FacilityOverviewComponent,
    NewPasswordComponent,
    LoginComponent,
    LogoutComponent,
    RegistrationConfirmationComponent,
    ResendCodeComponent,
    ForgotPasswordStep1Component,
    ForgotPassword2Component,
    RegisterComponent,
    MFAComponent,
    AboutComponent,
    HomeLandingComponent,
    HomeComponent,
    UseractivityComponent,
    JwtComponent,
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ToasterModule.forRoot(),
    MainMaterialModule,
    CoreModule,
    SharedModule,
    FormsModule,
    NgbModule.forRoot(),
    HttpModule,
    HttpClientModule,
    RoutesModule,
    AdminModule,
    AlertDetailsModule,
    AlertPageModule
  ],
  providers: [
    ApiService,
    CognitoUtil,
    AwsUtil,
    DynamoDBService,
    UserRegistrationService,
    UserLoginService,
    UserParametersService,
    AlertPageService
  ],
  bootstrap: [
    AppComponent
  ]
})

export class AppModule {
}
