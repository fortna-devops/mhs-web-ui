import { Component, OnInit } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';

const USERS_MENU_ITEMS = [
  {
    name: 'My Profile',
    routerLink: ['/admin/myprofile']
  },
  {
    name: 'SES',
    routerLink: ['/admin/ses']
  },
  {
    name: 'SNS',
    routerLink: ['/admin/sns']
  },
  {
    name: 'Create User',
    routerLink: ['/admin/register']
  }
];

const SENSOR_MENU_ITEMS = [
  {
    name: 'Sensors',
    routerLink: ['/admin/sensors']
  }
];

@Component({
  selector: 'app-admin-menu',
  templateUrl: './admin-menu.component.html',
  styleUrls: ['./admin-menu.component.scss']
})
export class AdminMenuComponent implements OnInit {
  public menuItems: Array<object> = [];

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
    if (this.router.url === '/admin/sensors') {
      this.menuItems = SENSOR_MENU_ITEMS;
    } else {
      this.menuItems = USERS_MENU_ITEMS;
    }

    this.router.events.subscribe(ev => {
      if (ev instanceof NavigationStart) {
        if (ev.url.indexOf('/admin/sensors') !== -1) {
          this.menuItems = SENSOR_MENU_ITEMS;
        } else {
          this.menuItems = USERS_MENU_ITEMS;
        }
      }
    });
  }
}
