import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { UsersModule } from './users/users.module';
import { SensorsModule } from './sensors/sensors.module';
import { RouterModule } from '@angular/router';
import { AdminMenuComponent } from './admin-menu/admin-menu.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    UsersModule,
    SensorsModule,
  ],
  declarations: [
    AdminComponent,
    AdminMenuComponent
  ]
})
export class AdminModule { }
