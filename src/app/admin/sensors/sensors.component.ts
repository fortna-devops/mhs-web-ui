import { Component, OnInit } from '@angular/core';
import { TopNavService } from '../../shared/top-nav/top-nav.service';
import { ApiService } from '../../api/api.service';
import { ToasterService } from 'angular2-toaster';
import { MatDialog } from '@angular/material';
import { AddSensorDialogComponent } from './add-sensor-dialog/add-sensor-dialog.component';
import { SensorsService } from './sensors.service';
import { ConfirmDialogComponent } from '../../shared/confirm-dialog/confirm-dialog.component';

const ADD_DIALOG_WIDTH = '500px';
const ERROR_STATUS = 'error';
const SUCCESS_STATUS = 'success';
const SENSOR_DELETED_SUCCESSFULLY = 'Sensor was deleted successfully.';
const SENSOR_ADDED_SUCCESSFULLY = 'Sensor was added successfully.';
const DELETE_DIALOG_WIDTH = '300px';
const DELETE_MESSAGE = 'Are you sure you want to delete this sensor?';

@Component({
  selector: 'app-sensors',
  templateUrl: './sensors.component.html',
  styleUrls: ['./sensors.component.scss']
})
export class SensorsComponent implements OnInit {
  public alertList: Array<any> = [];
  public isLoading: Boolean = false;

  constructor(
    private sensorsService: SensorsService,
    private topNav: TopNavService,
    private api: ApiService,
    private dialog: MatDialog,
    private toasterService: ToasterService
  ) { }

  ngOnInit() {
    this.getAlertData();
  }

  private getAlertData() {
    this.api.getAlertData(this.topNav.getSiteId())
      .subscribe(response => {
        this.alertList = response.data
          .map(item => {
            const thresholds = JSON.parse(item['thresholds']);
            const metrics = Object.keys(thresholds);

            metrics.forEach(metric => {
              item[metric] = item[metric] || [];
              // tslint:disable-next-line:forin
              for (const thresholdList in thresholds[metric]) {
                item[metric].push(`${thresholdList}: ${thresholds[metric][thresholdList]}`);
              }
            });
            return item;
          })
          .sort(this.sensorsService.sensorCompare);

      }, (error) => {
        this.toasterService.pop('error', '', `An error occurred. Please try again later`);
      });
  }

  public editSensor(alert) {
    const dialogRef = this.dialog.open(AddSensorDialogComponent, {
      data: {
        alert: alert,
        action: 'Edit sensor'
      },
      width: ADD_DIALOG_WIDTH
    });

    dialogRef.afterClosed().subscribe(sensor => {
      if (sensor) {
        // Add check is pristine
        this.sensorsService
          .editSensor(sensor)
          .then(editSensorResult => {
            if (editSensorResult && editSensorResult['code'] && editSensorResult['message']) {
              this.toasterService.pop(ERROR_STATUS, ``, `An error occurred. ${editSensorResult['message']}`);
              return;
            }
            this.getAlertData();
            this.toasterService.pop(SUCCESS_STATUS, ``, SENSOR_ADDED_SUCCESSFULLY);
          });
      }
    });
  }

  public addSensor() {
    const dialogRef = this.dialog.open(AddSensorDialogComponent, {
      data: {
        action: 'Add sensor'
      },
      width: ADD_DIALOG_WIDTH
    });

    dialogRef.afterClosed().subscribe(sensor => {
      if (sensor) {
        // Add check is pristine
        this.sensorsService
          .addSensor(sensor)
          .then(addSensorResult => {
            if (addSensorResult && addSensorResult['code'] && addSensorResult['message']) {
              this.toasterService.pop(ERROR_STATUS, ``, `An error occurred. ${addSensorResult['message']}`);
              return;
            }
            this.getAlertData();
            this.toasterService.pop(SUCCESS_STATUS, ``, SENSOR_ADDED_SUCCESSFULLY);
          });
      }
    });
  }

  public deleteSensor(sensor) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: { message: DELETE_MESSAGE },
      width: DELETE_DIALOG_WIDTH
    });
    dialogRef.afterClosed().subscribe(dialogConfirmation => {
      if (dialogConfirmation === 'confirm') {
        this.sensorsService
          .deleteSensor(sensor['sensor'])
          .then(deleteSensorResult => {
            if (deleteSensorResult && deleteSensorResult['code'] && deleteSensorResult['message']) {
              this.toasterService.pop(ERROR_STATUS, ``, `An error occurred. ${deleteSensorResult['message']}`);
              return;
            }
            this.getAlertData();
            this.toasterService.pop(SUCCESS_STATUS, ``, SENSOR_DELETED_SUCCESSFULLY);
          });
      }
    });
  }

}
