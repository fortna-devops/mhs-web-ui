import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SensorsComponent } from './sensors.component';
import { ToasterModule } from 'angular2-toaster';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MainMaterialModule } from '../../main-material.module';
import { AddSensorDialogComponent } from './add-sensor-dialog/add-sensor-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ToasterModule,
    RouterModule,
    NgbModule,
    MainMaterialModule
  ],
  declarations: [
    SensorsComponent,
    AddSensorDialogComponent,
  ],
  entryComponents: [
    AddSensorDialogComponent
  ]
})
export class SensorsModule { }
