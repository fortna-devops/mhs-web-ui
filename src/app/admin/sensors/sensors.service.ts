import { Injectable } from '@angular/core';
import * as AWS from 'aws-sdk';
import * as S3 from 'aws-sdk/clients/s3';
import { AuthService } from '../../core/auth.service';
import { TopNavService } from '../../shared/top-nav/top-nav.service';

const SITE_PATH = {
  'fedex-louisville': 'fedex-louisville/location/',
  'dhl-miami': 'dhl-miami/location/',
  'diverter': 'dev/location/'
};

@Injectable({
  providedIn: 'root'
})
export class SensorsService {
  private siteId: string;

  constructor(
    private authService: AuthService,
    private topNavService: TopNavService
  ) {
    AWS.config.update({
      accessKeyId: this.authService.getAccessKey(),
      secretAccessKey: this.authService.getSecretAccessKey()
    });
    this.siteId = this.topNavService.getSiteId();
  }

  public editSensor(sensorParams) {
    const s3 = new AWS.S3({ apiVersion: '2016-04-19', region: 'us-east-1' });
    sensorParams['thresholds'] = '`' + sensorParams['thresholds'] + '`';

    // Add "Do exist" check
    const request = {
      Body: `${Object.keys(sensorParams).join(',')}\r\n${Object.values(sensorParams).join(',')}`,
      Bucket: `mhspredict-site-${this.siteId}`,
      Key: `${SITE_PATH[this.siteId]}location_${sensorParams['sensor']}.csv`
    };

    return new Promise((resolve, reject) => {
      s3.putObject(request, function (err, data) {
        if (err) {
          resolve(err);
        } else {
          resolve(data);
        }
      });
    });
  }

  public addSensor(sensorParams) {
    const s3 = new AWS.S3({ apiVersion: '2016-04-19', region: 'us-east-1' });

    const request = {
      Body: `${Object.keys(sensorParams).join(',')}\r\n${Object.values(sensorParams).join(',')}`,
      Bucket: `mhspredict-site-${this.siteId}`,
      Key: `${SITE_PATH[this.siteId]}location_${sensorParams['sensor']}.csv`
    };

    // Add "do exist" check
    return new Promise((resolve, reject) => {
      s3.putObject(request, function (err, data) {
        if (err) {
          resolve(err);
        } else {
          resolve(data);
        }
      });
    });
  }

  public deleteSensor(sensor) {
    const s3 = new AWS.S3({ apiVersion: '2016-04-19', region: 'us-east-1' });

    const request = {
      Bucket: `mhspredict-site-${this.siteId}`,
      Key: `${SITE_PATH[this.siteId]}location_${sensor}.csv`
    };

    // Add "do exist" check
    return new Promise((resolve, reject) => {
      s3.deleteObject(request, function (err, data) {
        if (err) {
          resolve(err);
        } else {
          resolve(data);
        }
      });
    });
  }

  public sensorCompare(a , b) {
    return parseInt(a.sensor, 10) - parseInt(b.sensor, 10);
  }
}
