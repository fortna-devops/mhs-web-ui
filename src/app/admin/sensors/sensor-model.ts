export class SensorModel {
  constructor (
    public sensor?: string,
    public description?: string,
    public manufacturer?: string,
    public conveyor?: string,
    public location?: string,
    public equipment?: string,
    public m_sensor?: string,
    public standards_score?: string,
    public temp_score?: string,
    public vvrms_score?: string,
    public thresholds?: object,
  ) { }
}


