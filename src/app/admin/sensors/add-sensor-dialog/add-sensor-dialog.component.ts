import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SensorModel } from '../sensor-model';

@Component({
  selector: 'app-add-sensor-dialog',
  templateUrl: './add-sensor-dialog.component.html',
  styleUrls: ['./add-sensor-dialog.component.scss']
})
export class AddSensorDialogComponent implements OnInit {
  public verificationEmail: string;
  public sensor: SensorModel;
  public action: string;

  constructor(
    public dialogRef: MatDialogRef<AddSensorDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {

    if (data['alert'] && data['alert']['sensor']) {
      this.sensor = new SensorModel(
        data['alert']['sensor'],
        data['alert']['description'],
        data['alert']['manufacturer'],
        data['alert']['conveyor'],
        data['alert']['location'],
        data['alert']['equipment'],
        data['alert']['m_sensor'],
        data['alert']['standards_score'],
        data['alert']['temp_score'],
        data['alert']['vvrms_score'],
        data['alert']['thresholds']
      );
    } else {
      this.sensor = new SensorModel();
    }

    this.action = data['action'];
   }

  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
