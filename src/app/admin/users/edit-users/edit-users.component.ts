import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { EditUsersService } from './edit-users.service';
import { UserModel } from '../register/register-user.model';
import { ToasterService } from 'angular2-toaster';

const SUBSCRIPTION = {
  hour: 0,
  minute: 0,
  second: 0
};

@Component({
  selector: 'app-edit-users',
  templateUrl: './edit-users.component.html',
  styleUrls: ['./edit-users.component.scss']
})
export class EditUsersComponent implements OnInit {
  public username: string;
  public user: UserModel = new UserModel();
  public meridian = true;
  private useInitData: UserModel = new UserModel();

  constructor(
    private editUsersService: EditUsersService,
    private location: Location,
    private activatedRoute: ActivatedRoute,
    private toasterService: ToasterService
  ) { }

  ngOnInit() {
    this.activatedRoute.params
      .subscribe(params => {
        this.username = params['username'];

        this.editUsersService
          .getUser(this.username)
          .then((userData) => {
            this.user['name'] = userData['Username'];
            userData['UserAttributes']
              .map(attribut => {
                if (attribut['Name'].indexOf('custom') !== -1) {
                    attribut['Name'] = attribut['Name'].split(':')[1];
                    attribut['Value'] = this.isJsonString(attribut['Value']) ? JSON.parse(attribut['Value']) : attribut['Value'];
                  }
                  return attribut;
                })
                .forEach(item => {
                  this.user[item['Name']] = item['Value'];
                  this.useInitData[item['Name']] = item['Value'];
                  this.initUserAttributes();
                });
          }, (error) => {
            this.toasterService.pop('error', '', `An error occurred. Please try again later`);
          });
      });
  }

  edit() {
    if (this.isUnchangedFormData()) {
      this.toasterService.pop('warning', '', 'There is not any change.');
      return;
    }
    this.editUsersService
      .saveUser(this.user)
      .then((updatedUser) => {
        if (updatedUser && updatedUser['message']) {
          this.toasterService.pop('error', '', updatedUser['message']);
        } else {
          this.ngOnInit();
          this.toasterService.pop('success', '', 'User was updated successfully.');
        }
      }, (error) => {
        this.toasterService.pop('error', '', 'An error occurred. Please try again later');
      });
  }

  back() {
    this.location.back();
  }

  private isJsonString(value) {
    try {
        JSON.parse(value);
    } catch (e) {
        return false;
    }
    return true;
  }

  private initUserAttributes() {
    if (!this.user['subscription']) {
      this.user['subscription'] = SUBSCRIPTION;
    }
  }

  private isUnchangedFormData() {
    return this.useInitData['email'] === this.user['email'] &&
      this.useInitData['phone_number'] === this.user['phone_number'] &&
      this.useInitData['subscription'] === this.user['subscription'];
  }
}
