import { Injectable } from '@angular/core';
import { AuthService } from '../../../core/auth.service';
import * as CognitoIdentityServiceProvider from 'aws-sdk/clients/cognitoidentityserviceprovider';
import * as AWS from 'aws-sdk/global';

@Injectable({
  providedIn: 'root'
})
export class EditUsersService {

  constructor(
    private authService: AuthService
  ) { }

  public getUser(username) {
    const params = {
      'Username': username,
      'UserPoolId': 'us-east-1_eEombidgG'
    };

    AWS.config.update({
      accessKeyId: this.authService.getAccessKey(),
      secretAccessKey: this.authService.getSecretAccessKey()
    });

    const cognitoIdentityServiceProvider = new CognitoIdentityServiceProvider({apiVersion: '2016-04-19', region: 'us-east-1'});

    return new Promise((resolve, reject) => {
      cognitoIdentityServiceProvider.adminGetUser(params, function(err, data) {
        if (err) {
          resolve(err);
        } else {
          resolve(data);
        }
      });
    });
  }

  public saveUser(user) {
    const params = {
      'UserAttributes': [
        {
          'Name': 'phone_number',
          'Value': user.phone_number
        },
        {
          'Name': 'email',
          'Value': user.email
        },
        {
          'Name': 'custom:subscription',
          'Value': JSON.stringify(user.subscription)
        }
      ],
      'Username': user.name,
      'UserPoolId': 'us-east-1_eEombidgG'
    };

    AWS.config.update({
      accessKeyId: this.authService.getAccessKey(),
      secretAccessKey: this.authService.getSecretAccessKey()
    });

    const cognitoIdentityServiceProvider = new CognitoIdentityServiceProvider({apiVersion: '2016-04-19', region: 'us-east-1'});

    return new Promise((resolve, reject) => {
      cognitoIdentityServiceProvider.adminUpdateUserAttributes(params, function(err, data) {
        if (err) {
          resolve(err);
        } else {
          resolve(data);
        }
      });
    });
  }
}
