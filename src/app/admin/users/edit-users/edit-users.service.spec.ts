import { TestBed, inject } from '@angular/core/testing';

import { EditUsersService } from './edit-users.service';

describe('EditUsersService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EditUsersService]
    });
  });

  it('should be created', inject([EditUsersService], (service: EditUsersService) => {
    expect(service).toBeTruthy();
  }));
});
