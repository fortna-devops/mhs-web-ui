import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import { Router } from '@angular/router';
import { Parameters, GetParametersCallback } from './parameters-callback';
import { MatDialog } from '@angular/material';
import * as AWS from 'aws-sdk/global';
import * as CognitoIdentityServiceProvider from 'aws-sdk/clients/cognitoidentityserviceprovider';
import { LoggedInCallback, CognitoUtil } from '../../../service/cognito.service';
import { UserLoginService } from '../../../service/user-login.service';
import { UserParametersService } from '../../../service/user-parameters.service';
import { UserRegistrationService } from '../../../service/user-registration.service';
import { ToasterService } from 'angular2-toaster';
import { AuthService } from '../../../core/auth.service';
import { ConfirmDialogComponent } from '../../../shared/confirm-dialog/confirm-dialog.component';
import { environment } from '../../../../environments/environment';

const DELETE_DIALOG_WIDTH = '300px';
const DELETE_MESSAGE = 'Are you sure you want to delete this user?';

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.scss']
})
export class MyProfileComponent implements LoggedInCallback, OnInit {
  public parameters: Array<Parameters> = [];
  public cognitoId: String;
  public poolId: string;
  public pools: Array<any> = [];
  public users: Array<any> = [];
  public activity_events: Array<any> = [];
  public userGroups: Array<any> = [];
  public radioSelected: string = '';
  public activeGroup: string = '';
  private client: any;

  constructor(
    public router: Router,
    public userService: UserLoginService,
    public userParams: UserParametersService,
    public userRegistrationService: UserRegistrationService,
    public cognitoUtil: CognitoUtil,
    private dialog: MatDialog,
    private toasterService: ToasterService,
    private authService: AuthService
  ) {
    AWS.config.update({
      accessKeyId: this.authService.getAccessKey(),
      secretAccessKey: this.authService.getSecretAccessKey()
    });

    this.client = new CognitoIdentityServiceProvider({apiVersion: '2016-04-19', region: 'us-east-1'});
  }

  ngOnInit() {
    this.userGroups = this.authService.getUserGroups();
    this.getUsersByGroup(this.userGroups[0]);
    this.activeGroup = this.userGroups[0];
  }

  getListUserPools() {
    // TODO: move to the service
    const subscription = from(this.client.listUserPools({MaxResults: 10}).promise());

    subscription.subscribe(data => {
        this.pools = data['UserPools'];
      },
      error => {
        console.log(error);
      });
  }

  getUsersByGroup(group: string) {
    this.users = [];
    this.activity_events = [];

    // TODO: remove static userPoolId
    const params = {
      'UserPoolId': 'us-east-1_eEombidgG',
      'GroupName': group,
      'Limit' : 60
    };

    // TODO: move to the service
    const subscription = from(this.client.listUsersInGroup(params).promise());

    subscription.subscribe(data => {
        this.users = data['Users'];
      },
      error => this.toasterService.pop('error', '', 'An error occurred. Please try again later'));
  }

  getUsers(id: string) {
    this.poolId = id;
    this.users = [];
    this.activity_events = [];

    const params = {
      UserPoolId: this.poolId
    };
    // TODO: move to the service
    const subscription = from(this.client.listUsers(params).promise());

    subscription.subscribe(data => {
        this.users = data['Users'];
      },
      error => this.toasterService.pop('error', '', 'An error occurred. Please try again later'));
  }

  loadActivity(username: string, el) {
    const params = {
      UserPoolId: 'us-east-1_eEombidgG',
      Username: username
    };
    const subscription = from(this.client.adminListUserAuthEvents(params).promise());

    subscription.subscribe(data => {
      this.activity_events = data['AuthEvents'];

      if (this.activity_events.length === 0) {
        this.toasterService.pop('success', '', `No Activity found for a user: ${username}`);
      }

      if (this.activity_events.length > 0) {
        setTimeout(() => {
          el.scrollIntoView();
        }, 100);
      }
    },
    error => {
      this.toasterService.pop('error', '', 'An error occurred. Please try again later');
    });
  }

  destroyUser(nickname: string) {
    // TODO: temp solution this move to the service
    const toaster = this.toasterService;
    const updateUserList = this.updateUserList;
    const users = this.users;

    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {message: DELETE_MESSAGE},
      width: DELETE_DIALOG_WIDTH
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === 'confirm') {
        const params = {
          UserPoolId: environment.userPoolId,
          Username: nickname
        };

        // TODO: move to the service
        this.client.adminDeleteUser(params, function (err, data) {
          if (err) {
            toaster.pop('error', '', 'An error occurred. Please try again later');
          } else {
            updateUserList(nickname, users);
            toaster.pop('success', '', 'User was deleted');
          }
        });
      }
    });
  }

  updateUserList(nickname, users) {
    users.splice(users.findIndex(item => item.Username === nickname), 1);
  }

  isLoggedIn(message: string, isLoggedIn: boolean) {
    if (!isLoggedIn) {
      this.router.navigate(['/home/login']);
    } else {
      this.userParams.getParameters(new GetParametersCallback(this, this.cognitoUtil));
    }
  }

  selectUserGroup(userGroup) {
    this.getUsersByGroup(userGroup);
    this.activeGroup = userGroup;
  }

  editUser(username) {
    this.router.navigate([`/admin/myprofile/${username}`]);
  }
}
