export class UserModel {
  constructor (
    public name?: string,
    public email?: string,
    public phone_number?: string,
    public group?: Array<string>,
    public password?: string,
    public nickname?: string,
    public subscription?: object,
    public attributes = []
  ) { }
}
