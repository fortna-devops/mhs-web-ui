import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { UserModel } from './register-user.model';
import { ToasterService } from 'angular2-toaster';
import { CognitoCallback } from '../../../service/cognito.service';
import { UserRegistrationService } from '../../../service/user-registration.service';
import { AuthService } from '../../../core/auth.service';

const SUBSCRIPTION = {
  hour: 0,
  minute: 0,
  second: 0
};

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements CognitoCallback, OnInit {
  public registrationUser: UserModel = new UserModel();
  public errorMessage: string = null;
  public userGroups: Array<string> = [];
  public meridian = true;

  constructor(
    public userRegistrationService: UserRegistrationService,
    public router: Router,
    private location: Location,
    private toasterService: ToasterService,
    private authService: AuthService
  ) {
    this.registrationUser.group = ['DHL'];
    this.initUserAttributes();
  }

  ngOnInit() {
    this.userGroups = this.authService.getUserGroups();
  }

  register() {
    this.errorMessage = null;
    this.userRegistrationService
      .register(this.registrationUser, this)
      .then((result) => {

        if (result && result['code'] && result['message']) {
          this.toasterService.pop('error', '', result['message']);
        }

        if (result && result['User'] && result['User']['Username'] && this.registrationUser['group'].length > 0) {
          this.toasterService.pop('success', '', 'User was created');
          this.addUserToGroup(result['User'], this.registrationUser['group']);
        }

      }, (error) => {
        this.toasterService.pop('error', '', 'An error occurred. Please try again later');
      });
  }

  addUserToGroup(user, groups) {
    groups.forEach(group => {
      this.userRegistrationService
        .addUserToGroup(user, group)
        .then((result) => {
          this.toasterService.pop(`success`, ``, `User was added to group: ${group}.`);
        }, (error) => {
          this.toasterService.pop(`error`, ``, `An error occurred. Please try again later.`);
        });
    });
  }

  cognitoCallback(message: string, result: any) {
    if (message != null) {
        this.errorMessage = message;
    } else {
        this.router.navigate(['/home/confirmRegistration', result.user.username]);
    }
  }

  back() {
    this.location.back();
  }

  private initUserAttributes() {
    if (!this.registrationUser['subscription']) {
      this.registrationUser['subscription'] = SUBSCRIPTION;
    }
  }
}
