import { Injectable, Inject } from '@angular/core';
import * as CognitoIdentityServiceProvider from 'aws-sdk/clients/cognitoidentityserviceprovider';
import * as AWS from 'aws-sdk/global';
import { UserModel } from './register/register-user.model';
import { CognitoUtil } from '../../service/cognito.service';
import { AuthService } from '../../core/auth.service';


@Injectable()
export class UsersService {

  constructor(
    private authService: AuthService,
    @Inject(CognitoUtil) public cognitoUtil: CognitoUtil
  ) { }

  register(user: UserModel): void {
    const params = {
      UserPoolId: 'us-east-1_eEombidgG',
      Username: user.name,
      DesiredDeliveryMediums: [
        'EMAIL'
      ],
      ForceAliasCreation: true || false,
      MessageAction: 'SUPPRESS',
      TemporaryPassword: user.password,
      UserAttributes: [
        {
          Name: 'nickname',
          Value: user.name
        },
        {
          Name: 'email',
          Value: user.email
        }
      ]
    };
    AWS.config.update({
      accessKeyId: this.authService.getAccessKey(),
      secretAccessKey: this.authService.getSecretAccessKey()
    });

    const client = new CognitoIdentityServiceProvider({apiVersion: '2016-04-19', region: 'us-east-1'});
    client.adminCreateUser(params, (err, data) => {
      if (err) {
        console.log(err, err.stack);
      } else {
        console.log(data);
      }
    });
  }
}
