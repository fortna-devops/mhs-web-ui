import { Component, OnInit } from '@angular/core';
import { SnsService } from './sns.service';
import { ToasterService } from 'angular2-toaster';
import { MatDialog } from '@angular/material';
import { CreateSubscriptionDialogComponent } from './create-subscription-dialog/create-subscription-dialog.component';

const TOPIC = 'sensing_notifications';
const TOPIC_ARN = 'arn:aws:sns:us-east-1:286214959794:sensing_notifications';
const CREATE_DIALOG_WIDTH = '500px';
const ERROR_STATUS = 'error';
const SUCCESS_STATUS = 'success';
const SUBSCRIPTION_CREATED_SUCCESSFULLY = 'Subscription was created successfully.';
const GENERIC_ERROR_MESSAGE = 'An error occurred. Please try again later';

@Component({
  selector: 'app-sns',
  templateUrl: './sns.component.html',
  styleUrls: ['./sns.component.scss']
})
export class SnsComponent implements OnInit {
  public subscriptionsList: Array<object> = [];
  public topic: string = TOPIC;

  constructor(
    private snsService: SnsService,
    private dialog: MatDialog,
    private toasterService: ToasterService
  ) { }

  ngOnInit() {
    this.getListSubscriptions();
  }

  public createSubscription() {

    const dialogRef = this.dialog.open(CreateSubscriptionDialogComponent, {
      data: {
        topic: this.topic,
        topicArn: TOPIC_ARN
      },
      width: CREATE_DIALOG_WIDTH
    });
    dialogRef.afterClosed().subscribe(phone => {
      if (phone) {
        this.snsService
        .createSubscription(phone)
        .then(createSubscriptionResult => {
          if (createSubscriptionResult && createSubscriptionResult['code'] && createSubscriptionResult['message']) {
            this.toasterService.pop(ERROR_STATUS, ``, `An error occurred. ${createSubscriptionResult['message']}`);
            return;
          }
          this.getListSubscriptions();
          this.toasterService.pop(SUCCESS_STATUS, ``, SUBSCRIPTION_CREATED_SUCCESSFULLY);
        }, error => {
          this.toasterService.pop(ERROR_STATUS, '', GENERIC_ERROR_MESSAGE);
        });
      }
    });

  }

  private getListSubscriptions() {
    this.snsService
      .getListSubscriptions()
      .then(subscriptions => {
        this.subscriptionsList = subscriptions['Subscriptions'];
      }, error => {
        this.toasterService.pop(ERROR_STATUS, '', GENERIC_ERROR_MESSAGE);
      });
  }
}
