import { Injectable } from '@angular/core';
import * as SNS from 'aws-sdk/clients/sns';
import * as AWS from 'aws-sdk/global';
import { AuthService } from '../../../core/auth.service';

const TOPIC_ARN = 'arn:aws:sns:us-east-1:286214959794:sensing_notifications';

@Injectable({
  providedIn: 'root'
})
export class SnsService {

  constructor(
    private authService: AuthService
  ) {
    AWS.config.update({
      accessKeyId: this.authService.getAccessKey(),
      secretAccessKey: this.authService.getSecretAccessKey()
    });
  }

  public getListSubscriptions() {
    const params = {
      TopicArn: TOPIC_ARN,
      NextToken: null
    };

    const sns = new SNS({ apiVersion: '2016-04-19', region: 'us-east-1' });

    return new Promise((resolve, reject) => {
      sns.listSubscriptionsByTopic(params, function (err, data) {
        if (err) {
          resolve(err);
        } else {
          resolve(data);
        }
      });
    });
  }

  public createSubscription(phone) {
    const params = {
      Protocol: 'sms',
      TopicArn: TOPIC_ARN,
      Endpoint: phone
    };

    const sns = new SNS({ apiVersion: '2016-04-19', region: 'us-east-1' });

    return new Promise((resolve, reject) => {
      sns.subscribe(params, function (err, data) {
        if (err) {
          resolve(err);
        } else {
          resolve(data);
        }
      });
    });
  }
}
