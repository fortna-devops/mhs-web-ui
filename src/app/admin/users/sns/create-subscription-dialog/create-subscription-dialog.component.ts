import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';


@Component({
  selector: 'app-create-subscription-dialog',
  templateUrl: './create-subscription-dialog.component.html',
  styleUrls: ['./create-subscription-dialog.component.scss']
})
export class CreateSubscriptionDialogComponent implements OnInit {
  public subscriptionPhone: string;
  public topic: string;
  public topicArn: string;

  constructor(
    public dialogRef: MatDialogRef<CreateSubscriptionDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.topic = data['topic'];
    this.topicArn = data['topicArn'];
  }

  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
