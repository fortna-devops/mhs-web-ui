import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersComponent } from './users.component';
import { RouterModule } from '@angular/router';
import { MyProfileComponent } from './my-profile/my-profile.component';
import { RegisterComponent } from './register/register.component';
import { UsersService } from './users.service';
import { FormsModule } from '@angular/forms';
import { ToasterModule } from 'angular2-toaster';
import { EditUsersComponent } from './edit-users/edit-users.component';
import { EditUsersService } from './edit-users/edit-users.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SesComponent } from './ses/ses.component';
import { SesService } from './ses/ses.service';
import { VerifyNewEmailDialogComponent } from './ses/verify-new-email-dialog/verify-new-email-dialog.component';
import { SnsComponent } from './sns/sns.component';
import { SnsService } from './sns/sns.service';
import { CreateSubscriptionDialogComponent } from './sns/create-subscription-dialog/create-subscription-dialog.component';
import { MainMaterialModule } from '../../main-material.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ToasterModule,
    RouterModule,
    NgbModule,
    MainMaterialModule
  ],
  declarations: [
    UsersComponent,
    MyProfileComponent,
    RegisterComponent,
    EditUsersComponent,
    SesComponent,
    VerifyNewEmailDialogComponent,
    SnsComponent,
    CreateSubscriptionDialogComponent
  ],
  providers: [
    UsersService,
    EditUsersService,
    SesService,
    SnsService
  ],
  entryComponents: [
    VerifyNewEmailDialogComponent,
    CreateSubscriptionDialogComponent
  ]
})
export class UsersModule { }
