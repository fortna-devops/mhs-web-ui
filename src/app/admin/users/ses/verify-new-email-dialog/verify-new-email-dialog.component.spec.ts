import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifyNewEmailDialogComponent } from './verify-new-email-dialog.component';

describe('VerifyNewEmailDialogComponent', () => {
  let component: VerifyNewEmailDialogComponent;
  let fixture: ComponentFixture<VerifyNewEmailDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifyNewEmailDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifyNewEmailDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
