import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-verify-new-email-dialog',
  templateUrl: './verify-new-email-dialog.component.html',
  styleUrls: ['./verify-new-email-dialog.component.scss']
})
export class VerifyNewEmailDialogComponent implements OnInit {
  public verificationEmail: string;

  constructor(
    public dialogRef: MatDialogRef<VerifyNewEmailDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
