import { TestBed, inject } from '@angular/core/testing';

import { SesService } from './ses.service';

describe('SesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SesService]
    });
  });

  it('should be created', inject([SesService], (service: SesService) => {
    expect(service).toBeTruthy();
  }));
});
