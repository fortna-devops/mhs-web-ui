import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SesComponent } from './ses.component';

describe('SesComponent', () => {
  let component: SesComponent;
  let fixture: ComponentFixture<SesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
