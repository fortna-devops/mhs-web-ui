import { Component, OnInit } from '@angular/core';
import { SesService } from './ses.service';
import { MatDialog } from '@angular/material';
import { VerifyNewEmailDialogComponent } from './verify-new-email-dialog/verify-new-email-dialog.component';
import { ToasterService } from 'angular2-toaster';
import { ConfirmDialogComponent } from '../../../shared/confirm-dialog/confirm-dialog.component';

const CREATE_DIALOG_WIDTH = '500px';
const DELETE_DIALOG_WIDTH = '300px';
const DELETE_MESSAGE = 'Are you sure you want to delete this email address?';
const ERROR_STATUS = 'error';
const SUCCESS_STATUS = 'success';
const INDENTITY_DELETED_SUCCESSFULLY = 'Identity was deleted successfully.';
const INDENTITY_ADDED_SUCCESSFULLY = 'Identity was added successfully.';

@Component({
  selector: 'app-ses',
  templateUrl: './ses.component.html',
  styleUrls: ['./ses.component.scss']
})
export class SesComponent implements OnInit {
  public identitiesList: Array<object> = [];

  constructor(
    private sesService: SesService,
    private dialog: MatDialog,
    private toasterService: ToasterService,
  ) { }

  ngOnInit() {
    this.sesService
      .getListIdentities()
      .then(listIndent => {
        this.identitiesList = Object.keys(listIndent['VerificationAttributes'])
          .map(item => {
            return {
              name: item,
              status: listIndent['VerificationAttributes'][item]['VerificationStatus']
            };
          });
      }, error => {
        this.toasterService.pop('error', '', 'An error occurred. Please try again later');
      });
  }

  public verifyNewEmail() {
    const dialogRef = this.dialog.open(VerifyNewEmailDialogComponent, {
      data: {},
      width: CREATE_DIALOG_WIDTH
    });
    dialogRef.afterClosed().subscribe(email => {
      if (email) {
        this.sesService
          .verifyEmailAddress(email)
          .then(sesResult => {
            if (sesResult && sesResult['code'] && sesResult['message']) {
              this.toasterService.pop(ERROR_STATUS, ``, `An error occurred. ${sesResult['message']}`);
              return;
            }
            this.ngOnInit();
            this.toasterService.pop(SUCCESS_STATUS, ``, INDENTITY_ADDED_SUCCESSFULLY);
          });
      }
    });
  }

  public deleteIdentity(email) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: { message: DELETE_MESSAGE },
      width: DELETE_DIALOG_WIDTH
    });
    dialogRef.afterClosed().subscribe(dialogConfirmation => {
      if (dialogConfirmation === 'confirm') {
        this.sesService
          .deleteIdentity(email)
          .then(sesResult => {
            if (sesResult && sesResult['code'] && sesResult['message']) {
              this.toasterService.pop(ERROR_STATUS, ``, `An error occurred. ${sesResult['message']}`);
              return;
            }
            this.ngOnInit();
            this.toasterService.pop(SUCCESS_STATUS, ``, INDENTITY_DELETED_SUCCESSFULLY);
          });
      }
    });
  }
}

