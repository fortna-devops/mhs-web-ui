import { Injectable } from '@angular/core';

import * as SES from 'aws-sdk/clients/ses';
import * as AWS from 'aws-sdk/global';
import { AuthService } from '../../../core/auth.service';

@Injectable({
  providedIn: 'root'
})
export class SesService {

  constructor(
    private authService: AuthService
  ) {
    AWS.config.update({
      accessKeyId: this.authService.getAccessKey(),
      secretAccessKey: this.authService.getSecretAccessKey()
    });
  }

  public getListIdentities() {
    const that = this;

    const params = {
      IdentityType: 'EmailAddress',
      MaxItems: 123,
      NextToken: ''
    };

    const ses = new SES({ apiVersion: '2016-04-19', region: 'us-east-1' });

    return new Promise((resolve, reject) => {
      ses.listIdentities(params, function (err, data) {
        if (err) {
          resolve(err);
        } else {
          resolve(that.getVerificationAttributes(data['Identities']));
        }
      });
    });
  }

  public verifyEmailAddress(email) {
    const params = {
      EmailAddress: email
    };

    const ses = new SES({ apiVersion: '2016-04-19', region: 'us-east-1' });

    return new Promise((resolve, reject) => {
      ses.verifyEmailAddress(params, function (err, data) {
        if (err) {
          resolve(err);
        } else {
          resolve(data);
        }
      });
    });
  }

  public deleteIdentity(email) {
    const params = {
      Identity: email
    };

    const ses = new SES({ apiVersion: '2016-04-19', region: 'us-east-1' });

    return new Promise((resolve, reject) => {
      ses.deleteIdentity(params, function (err, data) {
        if (err) {
          resolve(err);
        } else {
          resolve(data);
        }
      });
    });
  }

  private getVerificationAttributes(identities) {
    const params = {
      Identities: identities
    };

    const ses = new SES({ apiVersion: '2016-04-19', region: 'us-east-1' });

    return new Promise((resolve, reject) => {
      ses.getIdentityVerificationAttributes(params, function (err, data) {
        if (err) {
          resolve(err);
        } else {
          resolve(data);
        }
      });
    });
  }
}
