import { Component } from '@angular/core';
import { TokenService } from './shared/token.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']

})
export class AppComponent {
  title = 'app';

  constructor(
    private tokenService: TokenService
  ) {
    tokenService.setUserGroups();
  }
}
