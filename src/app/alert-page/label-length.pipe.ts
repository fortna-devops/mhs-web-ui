import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'labelLength'
})
export class LabelLengthPipe implements PipeTransform {

  transform(value: string, length: any): any {
    if (value.length > length) {
      return `${value.substring(0, length)} ...`;
    }
    return value;
  }

}
