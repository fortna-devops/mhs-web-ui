import { Component, OnInit, Input, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { ApiService } from '../../api/api.service';
import { TopNavService } from '../../shared/top-nav/top-nav.service';
import { DetailInsightsService } from '../../alert-details/detail-insights/detail-insights.service';
import { ChartModel } from '../../alert-details/detail-insights/chart.model';
import { HelperService } from '../../shared/helper.service';
import { ActiveFiltersService } from '../../shared/active-filters.service';
import { ToasterService } from 'angular2-toaster';
import {
  PLC_ACTIVE_CHART_RULES,
  ACTIVE_CHART_RULES,
  SENSOR_CHART_TITLES
} from '../../alert-details/detail-insights/config-detail-insights';
import * as moment from 'moment';
import { Subscription } from 'rxjs';
import { MultySensorsChartService } from './multy-sensors-chart.service';
declare let d3: any;

const LOCATIONS_TEXT = [
  {
    location: 'Motor',
  },
  {
    location: 'Gearbox',
  },
  {
    location: 'Bearing Bracket - Far',
  },
  {
    location: 'Bearing Motor - Near',
  },
  {
    location: 'Bearing Motor - Far',
  },
  {
    location: 'Bearing - Near',
  },
  {
    location: 'Bearing - Far',
  },
  {
    location: 'Programming Logistic Controller'
  },
  {
    location: 'Inside/Outside Panel'
  },
  {
    location: 'Grapper'
  },
  {
    location: 'Structure'
  }
];

const COLORS = [
  '#505e7a',
  '#79C6C9',
  '#c99e79',
  '#79c982',
  '#799dc9',
  '#8779c9',
  '#c379c9',
  '#c979ba',
  '#c98e79',
  '#62507a',
  '#507a57'
];

@Component({
  selector: 'app-multy-sensors-chart',
  templateUrl: './multy-sensors-chart.component.html',
  styleUrls: ['./multy-sensors-chart.component.scss']
})
export class MultySensorsChartComponent implements OnInit, OnDestroy {
  @Input() alertData: Array<any>;
  @Input() alertConveyorList: {};
  @ViewChild('chartContainer') chartContainer: ElementRef;
  @ViewChild('sensorsSelect') sensorsSelect;

  public selectSensorCharts: Array<object> = SENSOR_CHART_TITLES;
  public dateRangeSelected: number[];
  public dateRangeMin: number;
  public dateRangeMax: number;
  public dateRangeSteps: number = 24 * 60 * 60 * 1000;
  public minDate: Date;
  public maxDate: Date;
  public showTime: boolean = true;
  public activeCharts: any = {};
  public sensorCharts = {
    data: [],
    d3Options: {}
  };
  public sensorPhotoURL: any;
  public sensor: string;
  public sensors: Array<string> = [];
  public prevSensors: Array<string> = [];
  public generalError: boolean = false;
  public disableDateSlider = false;
  public sensorChartList: Array<any> = [];
  public sensorChart: object;
  public activeChart: string;
  private chartSubscription: Subscription;
  private metrics: Array<string> = [];
  public selectedSensors: any;
  public selectSensors: Array<any> = [];
  public loading = false;
  public equipment: string;
  private sensorEquipment = {};

  constructor(
    private api: ApiService,
    private topNav: TopNavService,
    private detailInsightsService: DetailInsightsService,
    private helperService: HelperService,
    private activeFiltersService: ActiveFiltersService,
    private toasterService: ToasterService,
    private multySensorsChartService: MultySensorsChartService
  ) { }

  ngOnInit() {
    if (this.activeFiltersService.doExistActiveFilter('activeChart')) {
      this.activeChart = this.activeFiltersService.getActiveFilter('activeChart');
    } else {
      this.activeChart = 'Z_RMS_IPS';
    }

    this.setSensorSelectBox();
    this.setInitSensor();
    this.initDateRangeSlider();
    this.setLoadingStatus(true);
    this.setChartData();
  }

  ngOnDestroy() {
    this.activeFiltersService.removeActiveFilters();
  }

  public onChange(dateRange: any): void {
    this.dateRangeSelected = dateRange;
    this.activeFiltersService.setActiveFilter('dateRangeSelected', this.dateRangeSelected);
    this.sensorCharts = { data: [] , d3Options: {}};

    if (this.sensors.length === 0) {
      return;
    }

    this.setLoadingStatus(true);
    this.setChartData();
  }

  private setChartData() {
    const sensorRequest = {
      site: this.topNav.getSiteId(),
      metric: this.activeChart,
      sensor: this.sensors,
      timestamp_start: new Date(this.dateRangeSelected[0]),
      timestamp_end: new Date(this.dateRangeSelected[1])
    };

    this.api.getSensorChartData(sensorRequest)
      .subscribe(result => {
        this.disableDateSlider = false;
        this.setLoadingStatus(false);

        if (result['data'].length === 0) {
          this.sensorCharts['data'] = [];
          this.toasterService.pop('success', '', 'No Records found!');
          return;
        }

        result['data'] = this.setTimezoneOffset(result['data']);
        const { minSensorValue, maxSensorValue } = this.getMinMaxValue(result['data'], this.activeChart);
        this.populateSensorChart(result['data'], maxSensorValue);
        this.assemblySensorChart(minSensorValue, maxSensorValue);
        this.sensorChartList = [];
        this.sensorChart = {};
      }, (error) => {
        this.toasterService.pop('error', '', 'An error occurred. Please try again later');
        this.activeFiltersService.removeActiveFilters();
        window.location.reload();
      });
  }

  private setSensorSelectBox() {
    LOCATIONS_TEXT.forEach(item => {
      const sensorCollection = [];
      this.alertData.forEach(alert => {
        if (item.location === alert['location_text']) {
          sensorCollection.push({
            sensor: alert['sensor'],
            priority: alert['priority'],
            conveyor: alert['conveyor'],
            equipment: alert['equipment'],
            description: alert['description']
          });

          this.sensorEquipment[alert['sensor']] = alert['equipment'];
        }
      });
      if (sensorCollection.length > 0) {
        this.selectSensors.push({
          location: item.location,
          sensors: sensorCollection.sort(this.helperService.sortByTwoProperty('priority', 'sensor'))
        });
      }
    });
  }

  private setInitSensor() {
    if (this.activeFiltersService.doExistActiveFilter('sensors') && this.activeFiltersService.doExistActiveFilter('equipment')) {
      this.sensors = this.activeFiltersService.getActiveFilter('sensors');
      this.equipment = this.activeFiltersService.getActiveFilter('equipment');
      return;
    }

    if (this.selectSensors.length > 0 && this.selectSensors[0]['sensors'].length > 0) {
      this.sensors.push(this.selectSensors[0]['sensors'][0]['sensor']);
      this.equipment = this.selectSensors[0]['sensors'][0]['equipment'];
      this.activeFiltersService.setActiveFilter('sensors', this.sensors);
      this.activeFiltersService.setActiveFilter('equipment', this.equipment);
    }
  }

  private populateSensorChart(chartList, maxValue) {
    const dataSortedBySensor = {};

    this.sensorCharts['redLineList'] = [];
    this.sensorCharts['yellowLineList'] = [];
    this.sensorCharts['blueLineList'] = [];
    this.sensorCharts['orangeLineList'] = [];
    this.sensorCharts['charts'] = [];

    chartList.forEach(chartData => {
      chartData['sensors'].forEach(sensorChart => {
        if (sensorChart['values'][this.activeChart] >= 0) {
          if (this.dateRangeSelected[1] - this.dateRangeSelected[0] < 3 * this.dateRangeSteps) {
            this.showTime = true;
          } else {
            this.showTime = false;
          }

          if (sensorChart['values'] && sensorChart['values'][this.activeChart] !== undefined) {
            const val = sensorChart['scale'] ?
              (sensorChart['values'][this.activeChart] * sensorChart['scale']) :
              sensorChart['values'][this.activeChart];

            if (!dataSortedBySensor[sensorChart['sensor']]) {
              dataSortedBySensor[sensorChart['sensor']] = [{ x: chartData['read_time'], y: parseFloat(val) }];
            } else {
              dataSortedBySensor[sensorChart['sensor']].push({ x: chartData['read_time'], y: parseFloat(val) });
            }
          }
        }
      });
    });

    this.sensors.forEach(sensor => {
      if (dataSortedBySensor[sensor] && Array.isArray(dataSortedBySensor[sensor])) {
        dataSortedBySensor[sensor].sort(this.dateCompare);
      }
      this.sensorCharts['charts'].push({
        sensor: sensor,
        data: dataSortedBySensor[sensor]
      });
    });

    // const { startTime, endTime } = this.getStartEndTime(this.sensorCharts['charts']);

    // const requestArrProp = {
    //   siteId: this.topNav.getSiteId(),
    //   sensor: this.sensor,
    //   maxValue: maxValue,
    //   startTime: startTime,
    //   endTime: endTime
    // };

    // if (this.activeChart === 'Z_RMS_IPS' || this.activeChart === 'X_RMS_V_IPS' || this.activeChart === 'TEMP_F') {
    //   this.sensorCharts['redLineList'] = this.sensorCharts['redLineList']
    //     .concat(this.multySensorsChartService.getRedLineList(requestArrProp, this.activeChart));
    //   this.sensorCharts['yellowLineList'] = this.sensorCharts['yellowLineList']
    //     .concat(this.multySensorsChartService.getYellowLineList(requestArrProp, this.activeChart));
    //   this.sensorCharts['blueLineList'] = this.sensorCharts['blueLineList']
    //     .concat(this.multySensorsChartService.getBlueLineList(requestArrProp, this.activeChart));
    // }

    // Edge case: keep order
    // TODO: refactor
    // const isInOutPanel = this.alertData
    //   .filter((item) => item['location_text'] === 'Inside/Outside Panel')
    //   .some((item) => {
    //     return this.sensors.indexOf(item['sensor']) > -1;
    //   });

    // if (this.activeChart === 'TEMP_F' && isInOutPanel ) {
    //   const inOutPanelThresholdsProp = {
    //     maxValue: maxValue,
    //     startTime: startTime,
    //     endTime: endTime
    //   };

    //   this.sensorCharts['redLineList'] = this.sensorCharts['redLineList']
    //     .concat(this.multySensorsChartService.getTempInOutPanelRedLineList(inOutPanelThresholdsProp));
    //   this.sensorCharts['orangeLineList'] = this.sensorCharts['orangeLineList']
    //     .concat(this.multySensorsChartService.getTempInOutPanelOrangeLineList(inOutPanelThresholdsProp));
    // }

    // if (this.equipment === 'B') {
    //   if (this.activeChart === 'X_RMS_G' || this.activeChart === 'Z_RMS_G') {
    //     const bearingThresholdsProp = {
    //       maxValue: maxValue,
    //       startTime: startTime,
    //       endTime: endTime
    //     };

    //     this.sensorCharts['redLineList'] = this.sensorCharts['redLineList']
    //       .concat(this.multySensorsChartService.getBearingRedLineList(bearingThresholdsProp));
    //     this.sensorCharts['orangeLineList'] = this.sensorCharts['orangeLineList']
    //       .concat(this.multySensorsChartService.getBearingOrangeLineList(bearingThresholdsProp));
    //     this.sensorCharts['yellowLineList'] = this.sensorCharts['yellowLineList']
    //       .concat(this.multySensorsChartService.getBearingYellowLineList(bearingThresholdsProp));
    //   }
    // }
  }

  private dateCompare(a, b) {
    if (moment(a['x']) < moment(b['x'])) {
      return -1;
    }
    if (moment(a['x']) > moment(b['x'])) {
      return 1;
    }
    return 0;
  }

  private removeEmptyPLCChart() {
    if ((this.sensor).slice(0, 3) !== 'PLC') {
      return;
    }
    const sensorCharts = Object.keys(this.sensorCharts);
    sensorCharts.forEach(sensor => {
      if (this.activeCharts[sensor] === 'active' && this.sensorCharts[sensor]['data'].length > 0) {
        const isEmpty = this.sensorCharts[sensor]['data'].every(chart => +chart.y === 0);
        if (isEmpty) {
          delete this.sensorCharts[sensor];
        }
      }
    });
  }

  private assemblySensorChart(minValue, maxValue) {
    this.sensorCharts['d3Options'] = this.getLineChartD3Options(
      this.getTitle(this.activeChart),
      this.dateRangeSelected,
      this.showTime,
      parseFloat(minValue),
      parseFloat(maxValue)
    );

    this.setLinesData();
  }

  private setMetrics() {
    this.metrics = Object.keys(this.activeCharts);
  }

  private setCharts(activeChart) {
    if ((this.sensor).slice(0, 3) === 'PLC') {
      this.activeCharts = PLC_ACTIVE_CHART_RULES;
    } else {
      this.activeCharts = ACTIVE_CHART_RULES;
    }

    const activeChartsKeys = Object.keys(this.activeCharts);

    activeChartsKeys.forEach(activeChartsKey => {
      this.sensorCharts[activeChartsKey] = new ChartModel(
        activeChartsKey,
        this.getTitle(activeChartsKey)
      );
    });
  }

  private initDateRangeSlider() {
    if (this.activeFiltersService.doExistActiveDateFilter()) {
      this.dateRangeSelected = this.activeFiltersService.getActiveFilter('dateRangeSelected');
      this.minDate = this.activeFiltersService.getActiveFilter('minDate');
      this.maxDate = this.activeFiltersService.getActiveFilter('maxDate');
      this.dateRangeMin = this.activeFiltersService.getActiveFilter('dateRangeMin');
      this.dateRangeMax = this.activeFiltersService.getActiveFilter('dateRangeMax');
      return;
    }

    this.minDate = new Date(Date.now() - this.dateRangeSteps * 70);
    this.maxDate = new Date(Date.now());
    this.dateRangeMin = this.minDate.getTime();
    this.dateRangeMax = this.maxDate.getTime();
    this.dateRangeSelected = [this.dateRangeMax - this.dateRangeSteps * 1, this.dateRangeMax];
    this.activeFiltersService.setActiveFilter('dateRangeSelected', this.dateRangeSelected);
    this.activeFiltersService.setActiveFilter('minDate', this.minDate);
    this.activeFiltersService.setActiveFilter('maxDate', this.maxDate);
    this.activeFiltersService.setActiveFilter('dateRangeMin', this.dateRangeMin);
    this.activeFiltersService.setActiveFilter('dateRangeMax', this.dateRangeMax);
  }

  private errorHandler(error) {
    this.generalError = true;
    window.location.href = '.';
  }

  private setLoadingStatus(status) {
    this.loading = status;
    this.metrics.forEach(metric => {
      this.sensorCharts[metric].loading = status;
    });
  }

  private getTitle(activeChart) {
    const title = SENSOR_CHART_TITLES
      .find(chart => chart.key === activeChart);

    return title && title.title ? title.title : '';
  }

  getMinMaxValue(sensorChartList, sensor) {
    let minValues: number;
    let maxValues: number;

    sensorChartList.forEach(sensorChart => {
      sensorChart['sensors'].forEach(sensorData => {
        if (parseFloat(sensorData['values'][sensor]) > maxValues || maxValues === undefined) {
          maxValues = parseFloat(sensorData['values'][sensor]);
        }
        if (parseFloat(sensorData['values'][sensor]) < minValues || minValues === undefined) {
          minValues = parseFloat(sensorData['values'][sensor]);
        }
      });
    });

    return {
      minSensorValue: minValues,
      maxSensorValue: maxValues
    };
  }

  private getStartEndTime(sensorList) {
    let endTime: string;
    let startTime: string;

    if (sensorList.length === 0) {
      return {
        startTime: '',
        endTime: ''
      };
    }

    sensorList.forEach(item => {
      if (!item['data'] || !Array.isArray(item['data']) && item['data'].length === 0) {
        return {
          startTime: '',
          endTime: ''
        };
      }

      item['data'].forEach((value) => {
        if (moment(startTime).isAfter(moment(value['x'])) || !startTime) {
          startTime = value['x'];
        }
        if (moment(endTime).isBefore(moment(value['x'])) || !endTime) {
          endTime = value['x'];
        }
      });
    });

    return {
      startTime: startTime,
      endTime: endTime
    };
  }

  private setLinesData() {
    const finalCharts = [];

    this.sensorCharts['charts'].forEach((sensorData, index) => {
      finalCharts.push({
        values: sensorData['data'],
        key: `Sensor ${sensorData['sensor']}`,
        color: COLORS[index],
        area: false
      });
    });

    const chartExtraLints = [{
        values: this.sensorCharts['redLineList'],
        key: 'Out of compliance',
        color: '#A01507',
        area: false,
        id: 'redLineId'
      }, {
        values: this.sensorCharts['orangeLineList'],
        key: 'Caution',
        color: '#F6C143',
        area: false
      }, {
        values: this.sensorCharts['yellowLineList'],
        key: 'Unsatisfactory',
        color: '#B0B200',
        area: false
      }, {
        values: this.sensorCharts['blueLineList'],
        key: 'Satisfactory',
        color: '#056F97',
        area: false
      }
    ];
    chartExtraLints.forEach(item => {
      if (item.values && item.values.length > 0) {
        finalCharts.push(item);
      }
    });

    this.sensorCharts['data'] = finalCharts;
  }


  private getLineChartD3Options(
    title: string,
    dateRangeSelected: number[],
    showTime: boolean,
    minValue = 0,
    maxValue = 0): any {
    const lineChartD3Options = {
      chart: {
        showXAxis: true,
        showYAxis: true,
        showLegend: true,
        type: 'lineChart',
        height: 550,
        margin: {
          top: 20,
          right: 20,
          bottom: 40,
          left: 75
        },
        x: function (d) { return new Date(d.x); },
        y: function (d) { return d.y; },
        useInteractiveGuideline: true,
        xAxis: {
          axisLabel: 'Time',
          tickFormat: (d) => showTime ? d3.time.format('%I:%M %p')(new Date(d)) : d3.time.format('%b %d')(new Date(d))
        },
        yAxis: {
          axisLabel: title,
          tickFormat: (d) => d3.format('.2f')(d),
          axisLabelDistance: 10
        },
        forceY: [0, maxValue + (maxValue * 5 / 100)],
        xScale: d3.time.scale.utc()
      }
    };

    if (minValue && maxValue) {
      if (maxValue - minValue < 0.05) {
        lineChartD3Options.chart.yAxis['ticks'] = 0;
      } else {
        lineChartD3Options.chart.yAxis['ticks'] = 5;
      }
    }

    return lineChartD3Options;
  }

  public setSensorCharts(event) {
    this.setLoadingStatus(true);
    this.activeChart = event;
    this.activeFiltersService.setActiveFilter('activeChart', this.activeChart);
    this.setChartData();
  }

  public openedChange(isOpenEvent) {
    if (isOpenEvent) {
      this.prevSensors = this.sensors;
    }

    if (!isOpenEvent && this.hasSensorsChanged()) {
      if (this.sensors.length === 0) {
        this.sensorCharts['data'] = [];
        return;
      }

      this.setLoadingStatus(true);
      this.setChartData();
    }
  }

  public setSensors(event) {
    this.sensors = event;
    this.activeFiltersService.setActiveFilter('sensors', this.sensors);
    this.setEquipment();
  }

  private hasSensorsChanged() {
    return this.prevSensors.length !== this.sensors.length ||
      !this.prevSensors.sort().every((value, index) => value === this.sensors.sort()[index]);
  }

  public disableSensorSelect(sensor) {
    return this.sensors.length > 4 && !(this.sensors.indexOf(sensor) !== -1);
  }

  private setEquipment() {
    this.sensors.forEach(sensor => {
      this.equipment = this.sensorEquipment[sensor];
    });
  }

  private setTimezoneOffset(data) {
    return data.map(item => {
      item.read_time = moment(item.read_time).add(-(new Date().getTimezoneOffset()), 'minutes').format();
      return item;
    });
  }
}
