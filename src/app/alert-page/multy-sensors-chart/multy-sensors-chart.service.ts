import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MultySensorsChartService {

  constructor() { }

  public getRedLineList(data, sensorChart) {
    const redLineList = [];

    if (data.siteId === '2' && (data.sensor === '20' || data.sensor === '15')) {
      if (data.maxValue > 0.44) {
        redLineList.push({x: data.startTime, y: 0.44});
        redLineList.push({x: data.endTime, y: 0.44});
      }
    } else {
      if (data.maxValue > 0.28 && sensorChart !== 'TEMP_F') {
        redLineList.push({x: data.startTime, y: 0.28});
        redLineList.push({x: data.endTime, y: 0.28});
      }
    }

    if (sensorChart === 'TEMP_F' && data.maxValue > 170) {
      redLineList.push({x: data.startTime, y: 170});
      redLineList.push({x: data.endTime, y: 170});
    }

    return redLineList;
  }

  public getYellowLineList(data, sensorChart) {
    const yellowLineList = [];
    if (data.siteId === '2' && (data.sensor === '20' || data.sensor === '15')) {
      if (data.maxValue > 0.18) {
        yellowLineList.push({x: data.startTime, y: 0.18});
        yellowLineList.push({x: data.endTime, y: 0.18});
      }
    } else {
      if (data.maxValue > 0.11 && sensorChart !== 'TEMP_F') {
        yellowLineList.push({x: data.startTime, y: 0.11});
        yellowLineList.push({x: data.endTime, y: 0.11});
      }
    }

    if (sensorChart === 'TEMP_F' && data.maxValue > 150) {
      yellowLineList.push({x: data.startTime, y: 150});
      yellowLineList.push({x: data.endTime, y: 150});
    }

    return yellowLineList;
  }

  public getBlueLineList(data, sensorChart) {
    const blueLineList = [];

    if (sensorChart === 'TEMP_F') {
      return blueLineList;
    }

    if (data.siteId === '2' && (data.sensor === '20' || data.sensor === '15')) {
      if (data.maxValue > 0.07) {
        blueLineList.push({x: data.endTime, y: 0.07});
        blueLineList.push({x: data.startTime, y: 0.07});
      }
    } else {
      if (data.maxValue > 0.04) {
        blueLineList.push({x: data.endTime, y: 0.04});
        blueLineList.push({x: data.startTime, y: 0.04});
      }
    }
    return blueLineList;
  }

  public getTempInOutPanelRedLineList(data) {
    const redLineList = [];
    redLineList.push({ x: data.startTime, y: 140 });
    redLineList.push({ x: data.endTime, y: 140 });

    return redLineList;
  }

  public getTempInOutPanelOrangeLineList(data) {
    const orangeLineList = [];
    orangeLineList.push({ x: data.startTime, y: 122 });
    orangeLineList.push({ x: data.endTime, y: 122 });

    return orangeLineList;
  }

  public getBearingRedLineList(data) {
    const redLineList = [];

    if (data.maxValue > 6.1) {
      redLineList.push({x: data.startTime, y: 6.1});
      redLineList.push({x: data.endTime, y: 6.1});
    }

    return redLineList;
  }

  public getBearingOrangeLineList(data) {
    const orangeLineList = [];

    if (data.maxValue > 3.1) {
      orangeLineList.push({x: data.startTime, y: 3.1});
      orangeLineList.push({x: data.endTime, y: 3.1});
    }

    return orangeLineList;
  }

  public getBearingYellowLineList(data) {
    const yellowLineList = [];

    if (data.maxValue > 1.51) {
      yellowLineList.push({x: data.startTime, y: 1.51});
      yellowLineList.push({x: data.endTime, y: 1.51});
    }

    return yellowLineList;
  }
}
