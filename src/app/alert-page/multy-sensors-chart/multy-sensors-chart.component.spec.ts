import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultySensorsChartComponent } from './multy-sensors-chart.component';

describe('MultySensorsChartComponent', () => {
  let component: MultySensorsChartComponent;
  let fixture: ComponentFixture<MultySensorsChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultySensorsChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultySensorsChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
