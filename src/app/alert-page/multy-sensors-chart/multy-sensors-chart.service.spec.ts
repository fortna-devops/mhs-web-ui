import { TestBed, inject } from '@angular/core/testing';

import { MultySensorsChartService } from './multy-sensors-chart.service';

describe('MultySensorsChartService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MultySensorsChartService]
    });
  });

  it('should be created', inject([MultySensorsChartService], (service: MultySensorsChartService) => {
    expect(service).toBeTruthy();
  }));
});
