import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlertConveyorComponent } from './alert-conveyor.component';

describe('AlertConveyorComponent', () => {
  let component: AlertConveyorComponent;
  let fixture: ComponentFixture<AlertConveyorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlertConveyorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertConveyorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
