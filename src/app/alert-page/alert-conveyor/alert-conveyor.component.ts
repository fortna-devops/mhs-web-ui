import { Component, OnInit } from '@angular/core';
import { TopNavService } from '../../shared/top-nav/top-nav.service';
import { ApiService } from '../../api/api.service';
import { AlertPageService } from '../alert-page.service';
import { HelperService } from '../../shared/helper.service';
import { EquipmentIcon, LocationText } from '../alert-page.enum';
import { ActivatedRoute } from '@angular/router';
import { ToasterService } from 'angular2-toaster';


@Component({
  selector: 'app-alert-conveyor',
  templateUrl: './alert-conveyor.component.html',
  styleUrls: ['./alert-conveyor.component.scss']
})
export class AlertConveyorComponent implements OnInit {
  public alertList: Array<any> = [];
  private allAlerts: Array<any> = [];
  public modalMessage: string;
  private time = '24';
  private sort = 'priority';
  public modalVisible = false;
  public conveyor: string;

  constructor(
    private route: ActivatedRoute,
    private topNav: TopNavService,
    private api: ApiService,
    private alertService: AlertPageService,
    private helperService: HelperService,
    private toasterService: ToasterService
  ) {

    topNav.show();
    this.alertService.getTime()
      .subscribe(time => {
        this.time = time;
        if (this.time === 'all') {
          this.alertList = this.allAlerts.slice();
          return;
        }
        this.alertList = this.allAlerts
          .filter(alert => this.time === alert.equipment)
          .sort(this.helperService.sortByProperty(this.sort));
      });
    this.alertService.getSort()
      .subscribe(sort => {
        this.sort = sort;
        this.alertList.sort(this.helperService.sortByProperty(this.sort));
      });
    this.alertService.getModalMessage()
      .subscribe(modalMessage => {
        this.modalVisible = true;
        this.modalMessage = modalMessage;
      });
  }

  ngOnInit() {
    this.route.params
      .subscribe(params => {
        this.conveyor = params['conveyor'];

        this.api.getAlertData(this.topNav.getSiteId())
          .subscribe(response => {
            this.alertList = response.data
              .filter(item => item.conveyor === this.conveyor)
              .map(item => {
                item['priority'] = this.getPriority(item);
                item['equipment_icon'] = this.getEquipmentIcon(item);
                item['location_text'] = this.getLocationText(item.location, item.equipment);
                item['label'] = item['sensor'].toString().slice(0, 3);
                return item;
              })
              .sort(this.helperService.sortByProperty('priority'));
              this.allAlerts = this.alertList.slice();
          }, (error) => {
            this.toasterService.pop('error', '', `An error occurred. Please try again later`);
          });
    });
  }

  private getEquipmentIcon(item): string {
    let equipmentIcon: string;
    const isDark = (item['priority'] === 2) ? true : (item['priority'] === 4 ? true : false);

    if (item['conveyor'] === 'Outside Panel' || item['conveyor'] === 'Inside Panel') {
      return isDark ? EquipmentIcon.panelLight : EquipmentIcon.panelLight;
    }

    switch (item['equipment']) {
      case 'M':
        equipmentIcon = isDark ? EquipmentIcon.motorDark : EquipmentIcon.motorLight;
        break;
      case 'G':
        equipmentIcon = isDark ? EquipmentIcon.gearboxDark : EquipmentIcon.gearboxLight;
        break;
      case 'B':
        equipmentIcon = isDark ? EquipmentIcon.bearingDark : EquipmentIcon.bearingLight;
        break;
      case 'PLC':
        equipmentIcon = isDark ? EquipmentIcon.plsDark : EquipmentIcon.plsLight;
        break;
      case 'S':
        equipmentIcon = isDark ? EquipmentIcon.grapperDark : EquipmentIcon.grapperLight;
        break;
    }

    return equipmentIcon;
  }

  private getLocationText(location, equipment) {
    if (equipment !== 'PLC') {
      return LocationText[location];
    }
    return LocationText[equipment];
  }

  private getPriority(item: any): number {
    const score = item.standards_score;
    if (score === '3') {
      return 1;
    } else if (score === '2') {
      return 2;
    } else if (score <= '1') {
      return 3;
    }
    return 4;
  }
}
