import { Injectable } from '@angular/core';
import { Observable, Subject, BehaviorSubject } from 'rxjs';

@Injectable()
export class AlertPageService {
  private filter = new Subject<any>();
  private sort = new Subject<any>();
  private modalMessage = new Subject<any>();
  private alert = new BehaviorSubject<any>({});
  private alertInfo: object = {};

  public getSort(): Observable<any> {
    return this.sort.asObservable();
  }
  public setSort(sort: string): void {
    this.sort.next(sort);
  }

  public setFilter(filter: string): void {
    this.filter.next(filter);
  }

  public getTime(): Observable<any> {
    return this.filter.asObservable();
  }

  public setAlert(alert: any): void {
    this.alert.next(alert);
    this.alertInfo = alert;
  }

  public getAlert(): BehaviorSubject<any> {
    return this.alert;
  }

  public getAlertInfo() {
    return this.alertInfo;
  }

  public setModalMessage(message: any): void {
    this.modalMessage.next(message);
  }

  public getModalMessage(): Subject<any> {
    return this.modalMessage;
  }
}
