import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertPageService } from '../alert-page.service';
import { TopNavService } from '../../shared/top-nav/top-nav.service';

@Component({
  selector: 'app-info-card',
  templateUrl: './info-card.component.html',
  styleUrls: ['./info-card.component.scss']
})
export class InfoCardComponent implements OnInit {

  @Input() item: any;

  constructor(
    private router: Router,
    private alertService: AlertPageService,
    private topNav: TopNavService
  ) { }

  ngOnInit() { }

  public actionsClick(alert: any) {
    // console.log('actionsClick()');
    alert.actions = true;
    this.alertService.setAlert(alert);
  }

  public escalateClick(alert: any) {
    // console.log('escalateClick()');
    this.alertService.setModalMessage('Escalated Alert');
    // this.alertService.setAlert(alert);
  }

  public insightsClick(alert: any) {
    console.log('insightsClick()');
    alert.actions = false;
    this.alertService.setAlert(alert);
    this.router.navigateByUrl(`/alerts/details/${this.topNav.getSiteId()}/${alert.sensor}`);
  }
}
