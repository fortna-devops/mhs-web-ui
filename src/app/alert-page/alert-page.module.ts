import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AlertPageComponent } from './alert-page.component';
import { PageHeaderComponent } from './page-header/page-header.component';
import { SharedModule } from '../shared/shared.module';
import { InfoCardComponent } from './info-card/info-card.component';
import { PageHeaderService } from './page-header/page-header.service';
import { MainMaterialModule } from '../main-material.module';
import { ConveyorCardComponent } from './conveyor-card/conveyor-card.component';
import { AlertConveyorComponent } from './alert-conveyor/alert-conveyor.component';
import { ConveyorMapComponent } from './conveyor-map/conveyor-map.component';
import { MultySensorsChartComponent } from './multy-sensors-chart/multy-sensors-chart.component';
import { NouisliderModule } from 'ng2-nouislider';
import { NvD3Module } from 'ng2-nvd3';
import { ToasterModule } from 'angular2-toaster';
import { MultySensorsChartService } from './multy-sensors-chart/multy-sensors-chart.service';
import { LabelLengthPipe } from './label-length.pipe';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    MainMaterialModule,
    NouisliderModule,
    NvD3Module,
    ToasterModule
  ],
  declarations: [
    AlertPageComponent,
    PageHeaderComponent,
    InfoCardComponent,
    ConveyorCardComponent,
    AlertConveyorComponent,
    ConveyorMapComponent,
    MultySensorsChartComponent,
    LabelLengthPipe
  ],
  providers: [
    PageHeaderService,
    MultySensorsChartService
  ]
})
export class AlertPageModule { }
