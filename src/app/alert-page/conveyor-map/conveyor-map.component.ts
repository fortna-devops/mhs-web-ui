import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { TopNavService } from '../../shared/top-nav/top-nav.service';
import { HelperService } from '../../shared/helper.service';
import { AlertPageService } from '../alert-page.service';
import { ToasterService } from 'angular2-toaster';

@Component({
  selector: 'app-conveyor-map',
  templateUrl: './conveyor-map.component.html',
  styleUrls: ['./conveyor-map.component.scss']
})
export class ConveyorMapComponent implements OnInit {
  @ViewChild('layoutContainer') layoutContainer: ElementRef;
  @Input() alertConveyorList: {};

  private time: string = '24';
  private sort: string = 'priority';
  public alertList: Array<any> = [];
  private allAlerts: Array<any> = [];
  public modalMessage: string;
  public modalVisible: boolean = false;

  constructor(
    private router: Router,
    private topNav: TopNavService,
    private helperService: HelperService,
    private alertService: AlertPageService,
    private toasterService: ToasterService
  ) {
    this.alertService.getTime()
      .subscribe(time => {
        this.time = time;
        if (this.time === 'all') {
          this.alertList = this.allAlerts.slice();
          return;
        }
        this.alertList = this.allAlerts
          .filter(alert => this.time === alert.equipment)
          .sort(this.helperService.sortByProperty(this.sort));
      });
  }

  ngOnInit() {
    const that = this;
    // TODO: remove comments
    // const layoutMap = `./assets/images/layouts/blueprint-site-${this.topNav.getSiteId()}.svg`;
    const layoutMap = `/external/${this.topNav.getSiteId()}/layouts/blueprint-site-${this.topNav.getSiteId()}.svg`;
    d3.xml(layoutMap)
      .mimeType('image/svg+xml')
      .get(function (error, xml) {
        if (error) {
          const layoutUnderConstruction = `./assets/images/layouts/under-construction.svg`;
          d3.xml(layoutUnderConstruction)
            .mimeType('image/svg+xml')
            .get(function (errorUC, xmlUC) {
              that.layoutContainer.nativeElement['appendChild'](xmlUC.documentElement);
            });
        } else {

          const tooltip = d3.select('body')
            .append('div')
            .attr('class', 'conveyortooltip')
            .style('opacity', 0);

          that.layoutContainer.nativeElement['appendChild'](xml.documentElement);

          const zoom = d3.behavior
            .zoom()
            .translate([0, 0])
            .scale(1)
            .scaleExtent([1, 4])
            .on('zoom', () => {
              svg.attr(`transform`, `translate(${d3.event['translate']})scale(${d3.event['scale']})`);
            });

          const svg = d3
            .select(that.layoutContainer.nativeElement.children[0])
            .call(zoom);

          svg.selectAll('circle')
            .each(function (d, i) {
              d3.select(this)
                .style('fill', (item) => {
                  if (d3.select(this).attr('id') &&
                    that.alertConveyorList[d3.select(this).attr('id')] &&
                    that.alertConveyorList[d3.select(this).attr('id')]['color']) {
                    return that.alertConveyorList[d3.select(this).attr('id')]['color'];
                  } else {
                    return '#2c20cc';
                  }
                })
                .on('click', function () {
                  tooltip.transition().style('opacity', 0);
                  that.router.navigateByUrl(`/alerts/${d3.select(this).attr('id')}`);
                })
                .on('mouseover', function () {
                  d3.select(this).attr('stroke', 'gray');
                  d3.select(this).style('cursor', 'hand');
                  tooltip
                    .transition()
                    .duration(300)
                    .style('opacity', .9)
                    .style('z-index', 400);
                  tooltip.html(
                    `Conveyor: ${that.alertConveyorList[d3.select(this).attr('id')]['conveyor']} </br>
                  Label: ${that.alertConveyorList[d3.select(this).attr('id')]['label']} </br>
                  Type: ${that.alertConveyorList[d3.select(this).attr('id')]['location_text']}`
                  )
                    .style('left', (d3.event['pageX'] + 10) + 'px')
                    .style('top', (d3.event['pageY'] - 60) + 'px');
                })
                .on('mouseout', function () {
                  d3.select(this).attr('stroke', 'none');
                  tooltip
                    .transition()
                    .duration(500)
                    .style('opacity', 0)
                    .style('z-index', 0);
                });
            });
        }
      });
  }
}
