import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConveyorMapComponent } from './conveyor-map.component';

describe('ConveyorMapComponent', () => {
  let component: ConveyorMapComponent;
  let fixture: ComponentFixture<ConveyorMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConveyorMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConveyorMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
