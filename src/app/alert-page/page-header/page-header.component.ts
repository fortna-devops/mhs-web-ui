import { Component, OnInit } from '@angular/core';
import { AlertPageService } from '../alert-page.service';
import { TopNavService } from '../../shared/top-nav/top-nav.service';
import { PageHeaderService } from './page-header.service';

const TABS_FILTER = [{
  name: 'All',
  key: 'all'
}, {
  name: 'Motors',
  key: 'M'
}, {
  name: 'Gearboxes',
  key: 'G'
}, {
  name: 'Bearings',
  key: 'B'
}];

const VIEW_BY = [{
  name: 'Priority',
  key: 'priority'
}, {
  name: 'Conveyor',
  key: 'conveyor'
}, {
  name: 'Sensor Location',
  key: 'location'
}, {
  name: 'Sensor ID',
  key: 'sensor'
}];


@Component({
  selector: 'app-page-header',
  templateUrl: './page-header.component.html',
  styleUrls: ['./page-header.component.scss']
})
export class PageHeaderComponent implements OnInit {
  public site: string = '';
  public site_blueprint: string = '';
  public title: string = '';
  public filterTabs = TABS_FILTER;
  public selectedFilterTab = TABS_FILTER[0];
  public viewBy = VIEW_BY;
  public selectedViewBy = VIEW_BY[0];
  public showDropdown = false;
  public showModal = false;

  constructor(
    private alertService: AlertPageService,
    private topNav: TopNavService,
    private pageHeaderService: PageHeaderService
  ) { }

  ngOnInit() {
    this.title = this.pageHeaderService.getSiteDescription();
    this.site = this.topNav.getSiteId();
    this.site_blueprint = `/external/${this.topNav.getSiteId()}/blueprints/full.pdf`;
  }

  public selectFilter(tab) {
    this.selectedFilterTab = tab;
    this.alertService.setFilter(tab.key);
  }

  closeDropdown() {
    setTimeout(() => {
      this.showDropdown = false;
    }, 300);
  }

  public toggleDropdown() {
      this.showDropdown = !this.showDropdown;
  }

  public selectViewBy(item) {
    this.selectedViewBy = item;
    this.alertService.setSort(item.key);
  }
}
