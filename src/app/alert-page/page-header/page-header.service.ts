import { Injectable } from '@angular/core';
import { TokenService } from '../../shared/token.service';

@Injectable({
  providedIn: 'root'
})
export class PageHeaderService {

  constructor(
    private token: TokenService
  ) { }

  public getSiteDescription(): string {
    let site: object = {};
    const activeSiteId = this.token.getSiteId();
    const cookiesSiteList = this.token.getSiteList();

    if (cookiesSiteList && activeSiteId) {
      site = JSON.parse(cookiesSiteList).find(item => item.site_id === activeSiteId);
    }

    return (Object.keys(site).length > 0 && site['name']) ? site['name'] : '';
  }
}

