import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { AlertPageService } from '../alert-page.service';
import { TopNavService } from '../../shared/top-nav/top-nav.service';

@Component({
  selector: 'app-conveyor-card',
  templateUrl: './conveyor-card.component.html',
  styleUrls: ['./conveyor-card.component.scss']
})
export class ConveyorCardComponent implements OnInit {
  @Input() item: any;
  @Input() groupsName: string;

  public cardImage: string;

  constructor(
    private router: Router,
    private alertService: AlertPageService,
    private topNavService: TopNavService
  ) { }

  ngOnInit() {
    if (this.topNavService.getSiteId() === 'diverter') {
      if (this.item && this.item['location_text'] === 'Motor') {
        this.cardImage = './assets/images/ic_motor_dark.svg';
      } else {
        this.cardImage = './assets/images/inside-panel.svg';
      }
    } else {
      this.cardImage = './assets/images/conveyor-card.svg';
    }
  }

  public actionsClick(alert: any) {
    alert.actions = true;
    this.alertService.setAlert(alert);
  }

  public escalateClick(alert: any) {
    this.alertService.setModalMessage('Escalated Alert');
  }

  public goToAlertConveyor(alert) {
    this.alertService.setAlert(alert);
    this.router.navigateByUrl(`/alerts/${alert.conveyor}`);
  }
}
