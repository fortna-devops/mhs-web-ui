import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConveyorCardComponent } from './conveyor-card.component';

describe('ConveyorCardComponent', () => {
  let component: ConveyorCardComponent;
  let fixture: ComponentFixture<ConveyorCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConveyorCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConveyorCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
