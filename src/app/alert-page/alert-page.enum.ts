export enum EquipmentIcon {
  motorDark = '../assets/images/ic_motor_dark.svg',
  motorLight = '../assets/images/ic_motor_light.svg',
  gearboxDark = '../assets/images/ic_gearbox_dark.svg',
  gearboxLight = '../assets/images/ic_gearbox_light.svg',
  bearingDark = '../assets/images/ic_bearing_dark.svg',
  bearingLight = '../assets/images/ic_bearing_light.svg',
  plsLight = '../assets/images/ic_plc_light.svg',
  plsDark = '../assets/images/ic_plc_dark.svg',
  panelLight = '../assets/images/inside-panel.svg',
  grapperDark = '../assets/images/ic_grapper_dark.svg',
  grapperLight = '../assets/images/ic_grapper_light.svg'
}

export enum LocationText {
  M = 'Motor',
  G = 'Gearbox',
  B = 'Bearing Bracket - Far',
  BMN = 'Bearing Motor - Near',
  BMF = 'Bearing Motor - Far',
  BN = 'Bearing - Near',
  BF = 'Bearing - Far',
  PLC = 'Programming Logistic Controller',
  S = 'Grapper',
  VFD = 'Inside/Outside Panel',
  STRUCT = 'Structure'
}

