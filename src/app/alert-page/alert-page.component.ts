import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ApiService } from '../api/api.service';
import { AlertPageService } from './alert-page.service';
import { LocationText } from './alert-page.enum';
import { HelperService } from '../shared/helper.service';
import { TopNavService } from '../shared/top-nav/top-nav.service';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { ToasterService } from 'angular2-toaster';

const PRIORITY_COLORS = {
  '1': '#CC4021',
  '2': '#DDDF00',
  '3': '#28a745',
  '4': '#F1F1F1'
};

@Component({
  selector: 'app-alert-page',
  templateUrl: './alert-page.component.html',
  styleUrls: ['./alert-page.component.scss'],
  providers: []

})
export class AlertPageComponent implements OnInit {
  private time: string = '24';
  private sort: string = 'priority';
  public alertList: Array<any> = [];
  private allAlerts: Array<any> = [];
  public modalMessage: string;
  public modalVisible: boolean = false;
  public alertConveyorList = {};
  public showConveyorMap = false;
  public alertData: any;
  public selected = new FormControl(0);
  public strokeColor = '#ffffff';
  public groupsName: string;

  constructor(
    private topNav: TopNavService,
    private api: ApiService,
    private alertService: AlertPageService,
    private helperService: HelperService,
    private toasterService: ToasterService,
    private iconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer
  ) {
    iconRegistry.addSvgIcon('blueprint', sanitizer.bypassSecurityTrustResourceUrl('assets/images/blueprint-icon-tab.svg'));
    iconRegistry.addSvgIcon('list-icon', sanitizer.bypassSecurityTrustResourceUrl('assets/images/nine-squares-tab.svg'));
    iconRegistry.addSvgIcon('line-graph', sanitizer.bypassSecurityTrustResourceUrl('assets/images/line-graph.svg'));

    topNav.show();
    this.alertService.getTime()
      .subscribe(time => {
        this.time = time;
        if (this.time === 'all') {
          this.alertList = this.allAlerts.slice();
          return;
        }
        this.alertList = this.allAlerts
          .filter(alert => this.time === alert.equipment)
          .sort(this.helperService.sortByProperty(this.sort));
      });
  }

  ngOnInit() {
    this.selected.setValue(localStorage.getItem('activeTab') || '0');

    this.api.getAlertData(this.topNav.getSiteId())
      .subscribe(response => {
        this.groupsName = response['groups_name'];
        // TODO: refactor
        this.alertData = response.data
          .map((item) => {
            return {
              sensor: item['sensor'],
              conveyor: item['conveyor'],
              priority: this.getPriority(item),
              label: item['sensor'].toString().slice(0, 3),
              location_text: this.getLocationText(item.location, item.equipment),
              color: PRIORITY_COLORS[this.getPriority(item)],
              equipment: item['equipment'],
              description: item['description']
            };
          });

        response.data
          .map((item) => {
            return {
              conveyor: item['conveyor'],
              priority: this.getPriority(item),
              label: item['sensor'].toString().slice(0, 3),
              location_text: this.getLocationText(item.location, item.equipment),
              color: PRIORITY_COLORS[this.getPriority(item)]
            };
          })
          .sort(this.helperService.sortByProperty('priority'))
          .map((item) => {
            if (!this.alertConveyorList[item.conveyor] || this.alertConveyorList[item.conveyor]['priority'] < item.conveyor) {
              this.alertConveyorList[item.conveyor] = item;
            }
          });

        Object.keys(this.alertConveyorList).forEach(conveyor => {
          this.alertList.push(this.alertConveyorList[conveyor]);
        });
        this.showConveyorMap = true;
      }, (error) => {
        this.toasterService.pop('error', '', `An error occurred. Please try again later`);
      });
  }

  private getLocationText(location, equipment) {
    if (equipment !== 'PLC') {
      return LocationText[location];
    }
    return LocationText[equipment];
  }

  private getPriority(item: any): number {
    const score = item.standards_score;
    if (score === '3') {
      return 1;
    } else if (score === '2') {
      return 2;
    } else if (score <= '1') {
      return 3;
    }
    return 4;
  }

  selectTabIndex(event) {
    localStorage.setItem('activeTab', event);
    this.selected.setValue(event);
  }
}
