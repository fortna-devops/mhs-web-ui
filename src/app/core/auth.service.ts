import { Injectable } from '@angular/core';
import { TokenService } from '../shared/token.service';

@Injectable()
export class AuthService {

  constructor(
    private tokenService: TokenService
  ) { }

  getUserGroups() {
    return this.tokenService.getUserGroups();
  }

  isUserAdmin() {
    return this.tokenService.getUserGroups().indexOf('ADMIN') !== -1;
  }

  getAccessKey() {
    return this.tokenService.getAccessKey();
  }

  getSecretAccessKey() {
    return this.tokenService.getSecretAccessKey();
  }

  removeCookies() {
    this.tokenService.removeCookies();
  }
}
