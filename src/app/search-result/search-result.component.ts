import { Component, OnInit } from '@angular/core';
import { TopNavService } from '../shared/top-nav/top-nav.service';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.scss']
})
export class SearchResultComponent implements OnInit {

  constructor(private topNav: TopNavService) { }

  public horizontalBarChartOptions: any;
  public horizontalBarChartData: any;
  public doughnutChartLabels: string[] = ['Other', 'MODEL 487334-2'];
  public doughnutChartData: number[] = [33, 67];
  public doughnutChartType: string = 'doughnut';
  public doughnutChartColors: Array<any> = [{
    backgroundColor: ['#79C6C9', '#E9A853'],
  }];
  public doughnutChartOptions: any = {
    rotation: 3 * Math.PI,
    cutoutPercentage: 70,
    animation: {
      animateRotate: false,
      animateScale: false
    },
    // layout: {
    //   padding: {
    //     left: 100,
    //     right: 100,
    //     top: 0,
    //     bottom: 0
    //   }
    // },
    responsive: true,
    // maintainAspectRatio:true,
    legend: {
      display: true,
      position: 'left',
      labels: {
        fontSize: 16,
        fontFamily: 'Roboto',
        fontColor: '#828282'
      }
    }, legendCallback: function (chart) {
      // Return the HTML string here.
      console.log(chart);
      return '<p>test</p>';
    }
  };

  ngOnInit() {
    this.topNav.hide();
    this.topNav.setTitle('Search Results');
    let minDate: Date = new Date();
    minDate.setDate(new Date().getDate()-20);
    this.horizontalBarChartOptions = this.getLineChartD3Options('', [minDate.getTime(), new Date().getTime()]);
    this.horizontalBarChartData = [
      {
        "key": "Lot #7776731",
        "color": "#E9A853",
        "values": [
          {
            "x": "Motor 1",
            "y": 1502737061475-(2*24*60*60*1000)
          },
          {
            "x": "Motor 2",
            "y": 1502737061475-(3*24*60*60*1000)
          }
        ]
      },
      {
        "key": "Lot #776729",
        "color": "#79C6C9",
        "values": [
          
          {
            "x": "Motor 3",
            "y": 1502737061475-(4*24*60*60*1000*1000)
          },
          {
            "x": "Motor 4",
            "y": 1502737061475-(5*24*60*60*1000*1000)
          },
          {
            "x": "Motor 6",
            "y": 1502737061475-(2*24*60*60*1000*1000)
          },
          {
            "x": "Motor 12",
            "y": 1502737061475-(3*24*60*60*1000*1000)
          },
          {
            "x": "Motor 13",
            "y": 1502737061475-(5*24*60*60*1000*1000)
          },
          {
            "x": "Motor 17",
            "y": 1502737061475-(4*24*60*60*1000)
          }
        ]
      }
     ]
  }


  private getLineChartD3Options(title: string, dateRangeSelected: number[]): any {
    return {
      chart: {
        showControls: false,
        showXAxis: true,
        showYAxis: true,
        showLegend: false,
        groupSpacing:0.5,
        type: 'multiBarHorizontalChart',
        height: 450,
        margin: {
          top: 20,
          right: 20,
          bottom: 40,
          left: 55
        },
        x: function (d) { return d.x; },
        y: function (d) { return new Date(d.y); },
        useInteractiveGuideline: true,
        yAxis: {
          axisLabel: 'Days',
          tickFormat: function (d) { return d3.time.format('%b %d')(new Date(d)); }
        },
        // yAxis: {
        //   axisLabel: title,
        //   tickFormat: function (d) {
        //     return d3.format('d')(d);
        //   },
        //   axisLabelDistance: -10
        // },
        // xScale: d3.time.scale.utc()
      }
    };

  }

}
