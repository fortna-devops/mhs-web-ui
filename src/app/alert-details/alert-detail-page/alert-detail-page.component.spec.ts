import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlertDetailPageComponent } from './alert-detail-page.component';

describe('AlertDetailPageComponent', () => {
  let component: AlertDetailPageComponent;
  let fixture: ComponentFixture<AlertDetailPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlertDetailPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertDetailPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
