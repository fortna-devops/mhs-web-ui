import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Subscription } from 'rxjs';
import { AlertPageService } from '../../alert-page/alert-page.service';
import { ApiService } from '../../api/api.service';
import { LocationText } from '../../alert-page/alert-page.enum';
import { TopNavService } from '../../shared/top-nav/top-nav.service';

@Component({
  selector: 'app-alert-detail-page',
  templateUrl: './alert-detail-page.component.html',
  styleUrls: ['./alert-detail-page.component.scss']
})
export class AlertDetailPageComponent implements OnInit {
  id: string;
  private sub: any;
  public selectedTab = 0;
  private alertSubscription: Subscription;
  private modalMessageSubscription: Subscription;
  public modalMessage: string;
  public modalVisible: boolean;
  public alert: any = {};

  constructor(
    public topNav: TopNavService,
    private alertService: AlertPageService,
    private route: ActivatedRoute,
    private api: ApiService
  ) { }

  ngOnInit() {
    this.topNav.hide();

    this.modalMessageSubscription = this.alertService.getModalMessage()
    .subscribe(modalMessage => {
      this.modalVisible = true;
      this.modalMessage = modalMessage;
    });

    this.sub = this.route.params
      .subscribe(params => {
        this.id = params['id'];
        this.topNav.setTitle('Sensor ' + this.id);

        const alertInfo = this.alertService.getAlertInfo();

        if (Object.keys(alertInfo).length === 0) {
          this.api.getAlertData(this.topNav.getSiteId())
            .subscribe(response => {
              this.alert = response['data'].find((item) => item.sensor === params['id']);
              this.alert['priority'] = this.getPriority(this.alert);
              this.alert['location_text'] = this.getLocationText(this.alert['location'], this.alert['equipment']);
              this.selectedTab = this.alert['actions'] ? 1 : 0;
            }, (error) => (window.location.href = '.')
          );
        } else {
          this.alert = alertInfo;
          this.selectedTab = this.alert['actions'] ? 1 : 0;
        }
      });
  }

  insightsClick() {
    this.selectedTab = 0;
  }

  actionsClick() {
    this.selectedTab = 1;
  }

  // TODO: repeating/redundant getPriority -> movo to shares service
  private getPriority(item: any): number {
    const score = item.standards_score;
    if (score === '3') {
      return 1;
    } else if (score === '2') {
      return 2;
    } else if (score <= '1') {
      return 3;
    }
    return 4;
  }

  private getLocationText(location, equipment) {
    if (equipment !== 'PLC') {
      return LocationText[location];
    }
    return LocationText[equipment];
  }

  public getCssClass(tab) {
    const classes = [];
    if (this.selectedTab === tab) {
      classes.push('active');
    }
    if (this.alert.priority === 1 || this.alert.priority === 2 || this.alert.priority === 3) {
      classes.push(`priority-${this.alert.priority}`);
    }
    return classes;
  }
}
