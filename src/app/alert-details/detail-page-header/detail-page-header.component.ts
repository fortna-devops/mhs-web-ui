import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Subscription } from 'rxjs';
import { AlertPageService } from '../../alert-page/alert-page.service';
import { ApiService } from '../../api/api.service';

@Component({
  selector: 'app-detail-page-header',
  templateUrl: './detail-page-header.component.html',
  styleUrls: ['./detail-page-header.component.scss'],
  providers: []
})
export class DetailPageHeaderComponent implements OnInit {
  @Input() alertInfo: object;

  public id;
  public sub;
  public selectedTab = 0;
  public alert: any;
  public blueprintLink: any;
  public site: any;
  public plcLink: any;

  constructor(
    private alertService: AlertPageService,
    private route: ActivatedRoute,
    private api: ApiService
  ) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.site = +params['site'];
      this.id = +params['id'];
      this.alert = this.alertInfo;
      const conveyor = (this.alertInfo && this.alertInfo['conveyor']) ? this.alertInfo['conveyor'].substring(0, 5) : '';
      this.blueprintLink = `/external/${this.site}/blueprints/${conveyor}.pdf`;
      this.plcLink = '/alerts/details/' + this.site + '/PLC-' + this.alert.conveyor;
    });
  }

  public insightsClick(): void {
    this.selectedTab = 0;
  }

  public actionsClick(): void {
    this.selectedTab = 1;
  }

  public acknowledgeClick(): void {
    this.alertService.setModalMessage('Issue Acknowledged');
  }

  public escalateClick(): void {
    this.alertService.setModalMessage('Issue Escalated');
  }
}
