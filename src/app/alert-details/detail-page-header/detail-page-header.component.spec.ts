import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailPageHeaderComponent } from './detail-page-header.component';

describe('DetailPageHeaderComponent', () => {
  let component: DetailPageHeaderComponent;
  let fixture: ComponentFixture<DetailPageHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailPageHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailPageHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
