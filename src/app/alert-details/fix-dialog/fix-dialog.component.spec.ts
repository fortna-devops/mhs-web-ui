import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FixDialogComponent } from './fix-dialog.component';

describe('FixDialogComponent', () => {
  let component: FixDialogComponent;
  let fixture: ComponentFixture<FixDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FixDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FixDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
