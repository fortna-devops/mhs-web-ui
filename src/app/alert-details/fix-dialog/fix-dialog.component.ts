import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-fix-dialog',
  templateUrl: './fix-dialog.component.html',
  styleUrls: ['./fix-dialog.component.scss']
})
export class FixDialogComponent implements OnInit {

  @Input()
  public show: boolean = false;

  @Output()
  public showChange = new EventEmitter<boolean>();

  @Input()
  public message: string;

  @Input()
  public item: any;

  @Output()
  public onClose = new EventEmitter<any>();

  public showIssueModal: boolean = true;
  public showNoteModal: boolean = false;
  public issueResolved:boolean;

  constructor() { }

  ngOnInit() {
  }

  public toggleModal(): void {

    this.show = !this.show;
    this.showChange.emit(this.show);
    if(this.show==false){
      this.showIssueModal=true;
      this.showNoteModal=false;
    }
  }

  public resolveIssue(b:boolean):void{
    this.issueResolved=b;
  }

  toggleIssueModal() {
    this.showIssueModal = !this.showIssueModal;
    if(typeof(this.issueResolved)!='undefined'){
      this.onClose.emit({item:this.item,issueResolved:this.issueResolved});
    }
      
  }
  toggleNoteModal() {
    this.showNoteModal = !this.showNoteModal;
  }


  checked(flag, i) {
    if (flag) {

    }
  }
}
