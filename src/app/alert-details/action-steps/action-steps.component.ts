import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-action-steps',
  templateUrl: './action-steps.component.html',
  styleUrls: ['./action-steps.component.scss']
})
export class ActionStepsComponent implements OnInit {
  @Input() items: any[];
  @Input() titleKey: string;

  constructor() { }

  ngOnInit() {
  }
}
