import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailInsightsComponent } from './detail-insights.component';

describe('DetailInsightsComponent', () => {
  let component: DetailInsightsComponent;
  let fixture: ComponentFixture<DetailInsightsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailInsightsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailInsightsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
