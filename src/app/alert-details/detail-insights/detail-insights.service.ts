import { Injectable } from '@angular/core';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class DetailInsightsService {

  constructor() { }

  getMinMaxValues(sensorChartList) {
    const minValues = {};
    const maxValues = {};
    sensorChartList.forEach(item => {
      const keyList = Object.keys(item['values']);
      keyList.forEach(key => {
        if (parseFloat(item['values'][key]) > parseFloat(maxValues[key]) || maxValues[key] === undefined) {
          maxValues[key] = item['values'][key];
        }
        if (parseFloat(item['values'][key]) < parseFloat(minValues[key]) || minValues[key] === undefined) {
          minValues[key] = item['values'][key];
        }
      });
    });

    return {
      minValues: minValues,
      maxValues: maxValues
    };
  }

  getData(title, dataArr) {
    const finalCharts = [];
    finalCharts.push({
      values: dataArr['data'],
      key: title,
      color: '#79C6C9',
      area: false
    });

    const chartExtraLints = [{
      values: dataArr['redLineList'],
      key: 'Out of compliance',
      color: '#A01507',
      area: false
    }, {
      values: dataArr['yellowLineList'],
      key: 'Unsatisfactory',
      color: '#B0B200',
      area: false
    }, {
      values: dataArr['orangeLineList'],
      key: 'The risk assessment for catastrophic failure is high',
      color: '#F6C143',
      area: false
    }];

    chartExtraLints.forEach(item => {
      if (item.values && item.values.length > 0) {
        finalCharts.push(item);
      }
    });

    return finalCharts;
  }

  getSumOfMinutes(data) {
    let startTimestamp: moment.Moment = null;
    let prevTime: moment.Moment = null;

    return data
      .reduce((overallTime, item, index, array) => {

        if (startTimestamp === null) {
          startTimestamp = moment(item.x);
        } else {
          const currentTimestamp = moment(item.x);
          if ((index === array.length - 1) && startTimestamp !== null) {
            return overallTime + currentTimestamp.diff(startTimestamp, 'minutes');
          }
          const nextTimestamp = moment(array[index + 1].x);
          const numberOfMinutesForPeriod = currentTimestamp.diff(prevTime, 'minutes');
          const numberOfMinutesForNextPeriod = nextTimestamp.diff(currentTimestamp, 'minutes');
          if (numberOfMinutesForPeriod > 3 || (numberOfMinutesForNextPeriod > 3)) {
            const tempStartTimestamp = startTimestamp;
            startTimestamp = null;
            prevTime = null;
            return overallTime + currentTimestamp.diff(tempStartTimestamp, 'minutes');
          } else {
            prevTime = currentTimestamp;
          }
        }
        return overallTime;
      }, 0);
  }

  getLineChartD3Options(
    title: string,
    dateRangeSelected: number[],
    showTime: boolean,
    minValue = 0,
    maxValue = 0): any {
    const lineChartD3Options = {
      chart: {
        showXAxis: true,
        showYAxis: true,
        showLegend: false,
        type: 'lineChart',
        height: 250,
        margin: {
          top: 20,
          right: 20,
          bottom: 40,
          left: 75
        },
        x: function (d) { return new Date(d.x); },
        y: function (d) { return d.y; },
        useInteractiveGuideline: true,
        xAxis: {
          axisLabel: 'Time',
          tickFormat: (d) => showTime ? d3.time.format('%I:%M %p')(new Date(d)) : d3.time.format('%b %d')(new Date(d))
        },
        yAxis: {
          axisLabel: title,
          tickFormat: (d) => d3.format('.2f')(d),
          axisLabelDistance: 10
        },
        forceY: [0, maxValue + (maxValue * 20 / 100)],
        xScale: d3.time.scale.utc()
      }
    };

    if (minValue && maxValue) {
      if (maxValue - minValue < 0.05) {
        lineChartD3Options.chart.yAxis['ticks'] = 0;
      } else {
        lineChartD3Options.chart.yAxis['ticks'] = 5;
      }
    }

    return lineChartD3Options;
  }

  getDonutChartD3Options(title: string): any {
    return {
      chart: {
        showLegend: true,
        type: 'pieChart',
        height: 300,
        width: 300,
        margin: {
          top: 40,
          bottom: 40
        },
        x: function (d) { return d.label; },
        y: function (d) { return d.value; },
        showLabels: true,
        labelType: 'percent',
        donut: true,
        donutRatio: 0.35
      }
    };
  }

  public getRMSThresholds(sensor) {
    const redLineList: Array<object> = [];
    const orangeLineList: Array<object> = [];
    const yellowLineList: Array<object> = [];

    const redRmsAcceleration = sensor['thresholds']['rms_velocity_z']['red'];
    // if (redRmsAcceleration && sensor['maxValue'] > redRmsAcceleration) {
    if (redRmsAcceleration) {
      redLineList.push({ x: sensor['startTime'], y: redRmsAcceleration }, { x: sensor['endTime'], y: redRmsAcceleration });
    }

    const orangeRmsAcceleration = sensor['thresholds']['rms_velocity_z']['orange'];
    // if (orangeRmsAcceleration && sensor['maxValue'] > orangeRmsAcceleration) {
    if (orangeRmsAcceleration) {
      orangeLineList.push({ x: sensor['startTime'], y: orangeRmsAcceleration }, { x: sensor['endTime'], y: orangeRmsAcceleration });
    }

    const yellowRmsAcceleration = sensor['thresholds']['rms_velocity_z']['yellow'];
    // if (yellowRmsAcceleration && sensor['maxValue'] > yellowRmsAcceleration) {
    if (yellowRmsAcceleration) {
      yellowLineList.push({ x: sensor['startTime'], y: yellowRmsAcceleration }, { x: sensor['endTime'], y: yellowRmsAcceleration });
    }

    return {
      redLineList,
      orangeLineList,
      yellowLineList
    };
  }

  public getTempThresholds(sensor) {
    const redLineList: Array<object> = [];
    const orangeLineList: Array<object> = [];
    const yellowLineList: Array<object> = [];

    const redTemperature = sensor['thresholds']['temperature']['red'];
    // if (redTemperature && sensor['maxValue'] > redTemperature) {
    if (redTemperature) {
      redLineList.push({ x: sensor['startTime'], y: redTemperature }, { x: sensor['endTime'], y: redTemperature });
    }
    if (redTemperature && redLineList.length === 0 && sensor['location_text'] === 'Inside/Outside Panel') {
      redLineList.push({ x: sensor['startTime'], y: redTemperature }, { x: sensor['endTime'], y: redTemperature });
    }

    const orangeTemperature = sensor['thresholds']['temperature']['orange'];
    // if (orangeTemperature && sensor['maxValue'] > orangeTemperature) {
    if (orangeTemperature) {
      orangeLineList.push({ x: sensor['startTime'], y: orangeTemperature }, { x: sensor['endTime'], y: orangeTemperature });
    }
    if (orangeTemperature && orangeLineList.length === 0 && sensor['location_text'] === 'Inside/Outside Panel') {
      orangeLineList.push({ x: sensor['startTime'], y: orangeTemperature }, { x: sensor['endTime'], y: orangeTemperature });
    }

    const yellowTemperature = sensor['thresholds']['temperature']['yellow'];
    // if (yellowTemperature && sensor['maxValue'] > yellowTemperature) {
    if (yellowTemperature) {
      yellowLineList.push({ x: sensor['startTime'], y: yellowTemperature }, { x: sensor['endTime'], y: yellowTemperature });
    }

    return {
      redLineList,
      orangeLineList,
      yellowLineList
    };
  }
}
