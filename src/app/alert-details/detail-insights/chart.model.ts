export class ChartModel {
  constructor(
    public key: string,
    public title: string,
    public data: Array<any> = [],
    public raw_data: Array<any> = [],
    public redLineList: Array<any> = [],
    public yellowLineList: Array<any> = [],
    public blueLineList: Array<any> = [],
    public orangeLineList: Array<any> = []
  ) {}
}
