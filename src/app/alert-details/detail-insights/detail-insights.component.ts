import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import * as moment from 'moment';
import {
  ACTIVE_CHART_RULES,
  PLC_ACTIVE_CHART_RULES,
  DONUT_CHART,
  SENSOR_CHART_TITLES,
  BEARING_ACTIVE_CHART_RULES,
  GRAPPER_ACTIVE_CHART_RULES,
  ACTIVITY_TIME_THRESHOLD
} from './config-detail-insights';
import { ApiService } from '../../api/api.service';
import { TopNavService } from '../../shared/top-nav/top-nav.service';
import { DetailInsightsService } from './detail-insights.service';
import { ChartModel } from './chart.model';
import { ActiveFiltersService } from '../../shared/active-filters.service';
import { timer } from 'rxjs';
declare let d3: any;

const intervalTime = 120000;

@Component({
  selector: 'app-detail-insights',
  templateUrl: './detail-insights.component.html',
  styleUrls: ['./detail-insights.component.scss']
})
export class DetailInsightsComponent implements OnInit, OnDestroy {
  @Input() alertDetails: object;

  public dateRangeSelected: number[];
  public dateRangeMin: number;
  public dateRangeMax: number;
  public dateRangeSteps: number = 24 * 60 * 60 * 1000;
  public minDate: Date;
  public maxDate: Date;
  public showTime: boolean = true;
  public activeCharts: any = {};
  public sensorCharts = {};
  public sensorPhotoURL: any;
  public sensor: string;
  public generalError: boolean = false;
  public disableDateSlider = false;
  public sensorChartList: Array<any> = [];
  public donutChart = DONUT_CHART;
  public equipment: string;
  private thresholds: object = {};
  private getChartDataResponse: Array<any> = [];

  private chartSubscription: Subscription;
  private metrics: Array<string> = [];

  constructor(
    private api: ApiService,
    private topNav: TopNavService,
    private detailInsightsService: DetailInsightsService,
    private activeFiltersService: ActiveFiltersService
  ) { }

  ngOnInit() {
    this.sensor = this.alertDetails['sensor'];
    this.equipment = this.alertDetails['equipment'];
    this.thresholds = JSON.parse(this.alertDetails['thresholds']);
    this.sensorPhotoURL = `/external/${this.topNav.getSiteId()}/sensor-photos/${this.alertDetails['sensor']}.JPG`;

    this.setCharts(this.sensor, this.equipment);
    this.setMetrics();
    this.initDateRangeSlider();
    this.initDonutChart();
    this.setLoadingStatus(true);
    this.disableDateSlider = true;

    this.chartSubscription = this.api.getChartData(
      this.topNav.getSiteId(),
      this.sensor.toString(),
      this.metrics.join(),
      new Date(this.dateRangeSelected[0]),
      new Date(this.dateRangeSelected[1])
    )
      .subscribe(response => {
        this.getChartDataResponse = response['data'];
        this.donutChart['raw_data'] = [];
        const { minValues, maxValues } = this.detailInsightsService.getMinMaxValues(response['data']);
        this.disableDateSlider = false;

        response['data'] = this.setTimezoneOffset(response['data']);

        this.setLoadingStatus(false);
        this.populateSensorChart(response['data'], maxValues);
        this.populateDonutChart();
        // this.removeEmptyPLCChart();
        this.assemblySensorChart(minValues, maxValues);

        this.sensorChartList = [];
        for (const sensorChart in this.sensorCharts) {
          if (this.activeCharts[sensorChart] === 'active') {
            this.sensorChartList.push(this.sensorCharts[sensorChart]);
          }
        }

        if (moment(this.dateRangeSelected[0]).isSame(moment().subtract(1, 'day'), 'day')) {
          timer(intervalTime)
            .subscribe(i => {
              const dateRange = [moment().subtract(intervalTime, 'ms').valueOf(), moment().valueOf()];
              this.getIntervalChartData(dateRange);
            });
        }
      },
        (error) => {
          this.errorHandler(error);
        }
      );
  }

  ngOnDestroy() {
    this.chartSubscription.unsubscribe();
    this.activeFiltersService.removeActiveFilters();
  }

  private setMetrics() {
    this.metrics = Object.keys(this.activeCharts);
  }

  private setCharts(sensor, equipment) {

    if (equipment === 'S') {
      this.activeCharts = GRAPPER_ACTIVE_CHART_RULES;
    } else if (equipment === 'B') {
      this.activeCharts = BEARING_ACTIVE_CHART_RULES;
    } else if ((this.sensor).slice(0, 3) === 'PLC') {
      this.activeCharts = PLC_ACTIVE_CHART_RULES;
    } else {
      this.activeCharts = ACTIVE_CHART_RULES;
    }

    const activeChartsKeys = Object.keys(this.activeCharts);

    activeChartsKeys.forEach(activeChart => {
      this.sensorCharts[activeChart] = new ChartModel(
        activeChart,
        this.getTitle(activeChart)
      );
    });
  }

  private getTitle(activeChart) {
    const title = SENSOR_CHART_TITLES
      .find(chart => chart.key === activeChart);

    return title && title.title ? title.title : '';
  }

  private initDonutChart() {
    this.donutChart.d3Options = this.detailInsightsService.getDonutChartD3Options('Activity Time');
    this.donutChart.data = [];
    this.donutChart.loading = true;
  }

  private clearTimer() {
    if (this.chartSubscription === undefined) {
      return;
    }
    if ((this.chartSubscription && this.chartSubscription.closed)) {
      return;
    }

    this.chartSubscription.unsubscribe();
  }

  public onChange(dateRange: any): void {
    this.dateRangeSelected = dateRange;
    this.activeFiltersService.setActiveFilter('dateRangeSelected', this.dateRangeSelected);
    this.getChartDataResponse = [];
    this.setLoadingStatus(true);
    this.getIntervalChartData(this.dateRangeSelected);
  }

  private getIntervalChartData(dateRange) {
    this.disableDateSlider = true;
    this.clearTimer();

    this.chartSubscription = this.api.getIntervalChartData(
      this.topNav.getSiteId(),
      this.sensor.toString(),
      Object.keys(this.sensorCharts).join(),
      new Date(dateRange[0]),
      new Date(dateRange[1])
    )
      .subscribe(response => {
        this.getChartDataResponse.push(...(this.setTimezoneOffset(response['data'])));

        this.disableDateSlider = false;
        this.donutChart['raw_data'] = [];
        this.sensorChartList = [];
        this.sensorCharts = {};
        const { minValues, maxValues } = this.detailInsightsService.getMinMaxValues(this.getChartDataResponse);

        this.dateRangeSelected[1] = new Date(this.api.updatedEndDate).getTime();

        this.setLoadingStatus(false);
        this.populateSensorChart(this.getChartDataResponse, maxValues);
        this.populateDonutChart();
        // this.removeEmptyPLCChart();
        this.assemblySensorChart(minValues, maxValues);

        for (const sensorChart in this.sensorCharts) {
          if (this.activeCharts[sensorChart] === 'active') {
            this.sensorChartList.push(this.sensorCharts[sensorChart]);
          }
        }

        if (moment(this.dateRangeSelected[0]).isBefore(moment().subtract(1, 'day'), 'day')) {
          this.clearTimer();
        }
      },
        (error) => {
          this.errorHandler(error);
        }
      );
  }

  private setLoadingStatus(status) {
    if (Object.keys(this.activeCharts).length !== Object.keys(this.sensorCharts).length) {
      this.setCharts(this.sensor, this.equipment);
    }
    this.metrics.forEach(metric => {
      this.sensorCharts[metric].loading = status;
    });
  }

  private removeEmptyPLCChart() {
    if ((this.sensor).slice(0, 3) !== 'PLC') {
      return;
    }
    const sensorCharts = Object.keys(this.sensorCharts);
    sensorCharts.forEach(sensor => {
      if (this.activeCharts[sensor] === 'active' && this.sensorCharts[sensor]['data'].length > 0) {
        const isEmpty = this.sensorCharts[sensor]['data'].every(chart => +chart.y === 0);
        if (isEmpty) {
          delete this.sensorCharts[sensor];
        }
      }
    });
  }

  private assemblySensorChart(minValues, maxValues) {
    for (const sensorChart in this.sensorCharts) {
      if (this.activeCharts[sensorChart] === 'active') {
        this.sensorCharts[sensorChart]['d3Options'] = this.detailInsightsService.getLineChartD3Options(
          this.sensorCharts[sensorChart].title,
          this.dateRangeSelected,
          this.showTime,
          parseFloat(minValues[sensorChart]),
          parseFloat(maxValues[sensorChart])
        );

        this.sensorCharts[sensorChart]['data'] = this.detailInsightsService.getData(
          this.sensorCharts[sensorChart].title,
          this.sensorCharts[sensorChart]
        );
      }
    }
  }

  private populateDonutChart() {
    let activeMinutes = 0;
    let onHoldMinutes = 0;
    if (this.donutChart['raw_data'].length !== 0) {
      const raw = this.donutChart['raw_data'];
      for (let i = 1; i < raw.length; i++) {
        if (raw[i].y > ACTIVITY_TIME_THRESHOLD) {
          activeMinutes += moment(raw[i].x).diff(moment(raw[i - 1].x), 'minutes');
        } else {
          onHoldMinutes += moment(raw[i].x).diff(moment(raw[i - 1].x), 'minutes');
        }
      }
    }
    this.donutChart.data = [{
      'label': 'On Hours',
      'value': +(moment.duration(activeMinutes, 'minutes').asHours().toFixed(2))
    }, {
      'label': 'Off Hours',
      'value': +(moment.duration(onHoldMinutes, 'minutes').asHours().toFixed(2))
    }
    ];
  }

  private populateSensorChart(chartList, maxValues) {
    const startTime = {};
    const endTime = {};

    chartList.forEach(item => {
      const sensorCharts = Object.keys(this.sensorCharts);
      sensorCharts.forEach(sensorChart => {
        const dataArr: any = [];

        this.initThresholds(sensorChart);

        if (+item['values'][sensorChart] >= 0) {
          this.sensorCharts[sensorChart].loading = false;

          if (this.dateRangeSelected[1] - this.dateRangeSelected[0] < 3 * this.dateRangeSteps) {
            this.showTime = true;
          } else {
            this.showTime = false;
          }

          if (item.read_time && item['values'][sensorChart] !== undefined) {
            if (this.activeCharts[sensorChart] === 'active') {
              const val = sensorChart['scale'] ? item['values'][sensorChart] * sensorChart['scale'] : item['values'][sensorChart];
              this.sensorCharts[sensorChart]['data'].push({ x: item.read_time, y: parseFloat(val) });
            }
            if (sensorChart === 'Z_RMS_IPS' && this.activeCharts['conveyor_on'] === 'pieChart') {
              this.donutChart['raw_data'].push({ x: item.read_time, y: item['values']['Z_RMS_IPS'] });
            }
          }

          if (this.sensorCharts[sensorChart]['data'].length === 1) {
            startTime[sensorChart] = item.read_time;
          }
          endTime[sensorChart] = item.read_time;
        }
      });
    });

    Object.keys(this.sensorCharts).forEach(sensorChart => {
      const sensor = {
        siteId: this.topNav.getSiteId(),
        sensor: this.sensor,
        maxValue: maxValues[sensorChart],
        startTime: startTime[sensorChart],
        endTime: endTime[sensorChart],
        thresholds: this.thresholds,
        sensorChart: sensorChart,
        location_text: this.alertDetails['location_text']
      };

      if (sensorChart === 'Z_RMS_IPS' || sensorChart === 'X_RMS_V_IPS') {
        const { redLineList, orangeLineList, yellowLineList } = this.detailInsightsService.getRMSThresholds(sensor);
        this.sensorCharts[sensorChart]['redLineList'] = redLineList;
        this.sensorCharts[sensorChart]['orangeLineList'] = orangeLineList;
        this.sensorCharts[sensorChart]['yellowLineList'] = yellowLineList;
      }

      if (sensorChart === 'TEMP_F') {
        const { redLineList, orangeLineList, yellowLineList } = this.detailInsightsService.getTempThresholds(sensor);
        this.sensorCharts[sensorChart]['redLineList'] = redLineList;
        this.sensorCharts[sensorChart]['orangeLineList'] = orangeLineList;
        this.sensorCharts[sensorChart]['yellowLineList'] = yellowLineList;
      }

    });
  }

  private initDateRangeSlider() {
    if (this.activeFiltersService.doExistActiveDateFilter()) {
      this.dateRangeSelected = this.activeFiltersService.getActiveFilter('dateRangeSelected');
      this.minDate = this.activeFiltersService.getActiveFilter('minDate');
      this.maxDate = this.activeFiltersService.getActiveFilter('maxDate');
      this.dateRangeMin = this.activeFiltersService.getActiveFilter('dateRangeMin');
      this.dateRangeMax = this.activeFiltersService.getActiveFilter('dateRangeMax');
      return;
    }

    this.minDate = new Date(Date.now() - this.dateRangeSteps * 21);
    this.maxDate = new Date(Date.now());
    this.dateRangeMin = new Date(this.minDate.getTime()).setHours(0, 0, 0, 0);
    this.dateRangeMax = new Date(this.maxDate.getTime()).setHours(23, 59, 59, 999);
    this.dateRangeSelected = [
      new Date(this.dateRangeMax - this.dateRangeSteps * 1).setHours(0, 0, 0, 0),
      this.dateRangeMax
    ];

    this.activeFiltersService.setActiveFilter('dateRangeSelected', this.dateRangeSelected);
    this.activeFiltersService.setActiveFilter('minDate', this.minDate);
    this.activeFiltersService.setActiveFilter('maxDate', this.maxDate);
    this.activeFiltersService.setActiveFilter('dateRangeMin', this.dateRangeMin);
    this.activeFiltersService.setActiveFilter('dateRangeMax', this.dateRangeMax);
  }

  public showDonutChart() {
    return this.donutChart.data.some(item => item.value !== 0);
  }

  private errorHandler(error) {
    this.generalError = true;
    window.location.href = '.';
  }

  private initThresholds(sensorChart) {
    this.sensorCharts[sensorChart]['redLineList'] = [];
    this.sensorCharts[sensorChart]['yellowLineList'] = [];
    this.sensorCharts[sensorChart]['blueLineList'] = [];
    this.sensorCharts[sensorChart]['orangeLineList'] = [];
  }

  private setTimezoneOffset(data) {
    return data.map(item => {
      item.read_time = moment(item.read_time).add(-(new Date().getTimezoneOffset()), 'minutes').format();
      return item;
    });
  }
}
