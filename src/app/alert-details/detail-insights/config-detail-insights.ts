export const ACTIVE_CHART_RULES = {
  Z_RMS_IPS: 'active',
  X_RMS_V_IPS: 'active',
  X_RMS_G: 'active',
  TEMP_F: 'active',
  conveyor_on: 'pieChart'
};

export const PLC_ACTIVE_CHART_RULES = {
  belt_speed: 'active',
  motor_current: 'active',
  motor_freq: 'active',
  conveyor_on: 'pieChart'
};

export const BEARING_ACTIVE_CHART_RULES = {
  Z_RMS_IPS: 'active',
  X_RMS_V_IPS: 'active',
  X_RMS_G: 'active',
  Z_RMS_G: 'active',
  TEMP_F: 'active',
  conveyor_on: 'pieChart'
};

export const GRAPPER_ACTIVE_CHART_RULES = {
  Z_RMS_IPS: 'active',
  X_RMS_V_IPS: 'active',
  X_RMS_G: 'active',
  Z_RMS_G: 'active',
  conveyor_on: 'pieChart'
};

export const DONUT_CHART = {
  title: 'Activity Time',
  loading: false,
  raw_data: [],
  data: [],
  d3Options: {}
};

export const ACTIVITY_TIME_THRESHOLD = 0.02


export const SENSOR_CHART_TITLES = [
  {
    key: 'Z_RMS_IPS',
    title: 'Vibration: Z-Axis RMS Velocity (in/s)'
  },
  {
    key: 'X_RMS_V_IPS',
    title: 'Vibration: X-Axis RMS Velocity (in/s)'
  },
  {
    key: 'X_RMS_G',
    title: 'Vibration: X-Axis RMS Acceleration (G)'
  },
  {
    key: 'Z_RMS_G',
    title: 'Vibration: Z-Axis RMS Acceleration (G)'
  },
  {
    key: 'TEMP_F',
    title: 'Temperature (F)'
  },
  {
    key: 'conveyor_on',
    title: 'Percent time on (%)'
  },
  {
    key: 'belt_speed',
    title: 'Belt Speed (feet/min)'
  },
  {
    key: 'motor_current',
    title: 'Motor Current (Amps)'
  },
  {
    key: 'motor_freq',
    title: 'Motor Frequency (Hz)'
  }
];
