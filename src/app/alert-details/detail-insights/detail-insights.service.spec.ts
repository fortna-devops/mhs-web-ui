import { TestBed, inject } from '@angular/core/testing';

import { DetailInsightsService } from './detail-insights.service';

describe('DetailInsightsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DetailInsightsService]
    });
  });

  it('should be created', inject([DetailInsightsService], (service: DetailInsightsService) => {
    expect(service).toBeTruthy();
  }));
});
