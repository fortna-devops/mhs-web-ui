import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { ApiService } from '../../api/api.service';
import { AlertPageService } from '../../alert-page/alert-page.service';
import { TwilioService } from '../../shared/twilio.service';

@Component({
  selector: 'app-detail-actions',
  templateUrl: './detail-actions.component.html',
  styleUrls: ['./detail-actions.component.scss']
})
export class DetailActionsComponent implements OnInit {
  @Input() alertDetails: object;
  private alertSubscription: Subscription;
  private alert: any;
  public actionsAddress: any[];
  public actionsTroubleshoot1: any[];
  public actionsTroubleshoot2: any[];
  public actionsMotors: any[];
  public actionsGearboxes: any[];
  public actionsType: string;
  public actions: any[];
  public actionsTitle: string;
  public quickFix: any;
  public topFix: any;
  public sensor: string;
  public location_text: string;

  constructor(
    private api: ApiService,
    private twilio: TwilioService,
    private alertService: AlertPageService
  ) {
    this.alertSubscription = this.alertService.getAlert()
      .subscribe(alert => {
        this.alert = alert;
      });
  }

  ngOnInit() {
    this.sensor = this.alertDetails['sensor'];
    this.location_text = this.alertDetails['location_text'];
    this.api.getActionsData().subscribe(data => {
      // Read the result field from the JSON response.

      for (var item of data.results) {

        if (item.type == 'address') {
          this.actionsAddress = item.steps;
        } else if (item.type == 'troubleshoot' && typeof (this.actionsTroubleshoot1) == 'undefined') {
          this.actionsTroubleshoot1 = item.steps;
        } else if (item.type == 'troubleshoot') {
          this.actionsTroubleshoot2 = item.steps;
        } else if (item.type == 'Motor') {
          this.actionsMotors = item.steps;
        }else if(item.type == 'Gearbox') {
          this.actionsGearboxes = item.steps;
        }
      }

      if (typeof (this.alert) != 'undefined') {
        this.getActionsType(this.alert);
      }


    });
  }
  teamData = [
    {
      name: "Maintenance Manager",
      imageUrl: "./assets/images/man.png",
      checked: false,
      assigned: false
    },
    {
      name: "Technician",
      imageUrl: "./assets/images/man.png",
      checked: false,
      assigned: false
    },
    {
      name: "Facility Manager",
      imageUrl: "./assets/images/man.png",
      checked: false,
      assigned: false
    },
  ]
  partsData = [
    {
      name: "Part #586032",
      imageUrl: "./assets/images/part1.png",
      status: "Low Inventory! 12 units remaining",
      info: "High impact if needed for replacement"
    },
    {
      name: "Part #239736",
      imageUrl: "./assets/images/part2.png",
      status: "Low Inventory! 8 units remaining",
      info: "High impact if needed for replacement"
    },
    {
      name: "Part #128321",
      imageUrl: "./assets/images/part3.png",
      status: "Low Inventory! 6 units remaining",
      info: "High impact if needed for replacement"
    }
  ]
  showModal = false;
  showIssueModal = false;
  showNoteModal = false;
  showNotificationModal = false;
  showText = true;
  issueResolved = false;
  assignedTeammate = [];
  public actionItem: any = {};

  assignTeammate() {
    for (var i = this.teamData.length - 1; i >= 0; i--) {
      if (this.teamData[i].checked) {
        this.teamData[i].assigned = true;
        this.assignedTeammate.push(this.teamData[i].name);
        this.teamData.splice(i, 1);
      }
    }
    this.twilio.sendSMS(this.alert).subscribe(data => {
      // console.log(data);

    });
    this.twilio.sendSMS2(this.alert).subscribe(data => {
      // console.log(data);

    });
  }

  public getActionsType(alert) {
    this.actionsType = alert.actions_type;
    if (this.actionsType == 'A') {
      this.actionsTitle = "Address this Issue";
      this.actions = this.actionsAddress;
    } else if (this.actionsType == 'T1') {
      this.actionsTitle = "Troubleshoot this Issue";
      this.actions = this.actionsTroubleshoot1;
    } else if (this.actionsType == 'T2') {
      this.actionsTitle = "Troubleshoot this Issue";
      this.actions = this.actionsTroubleshoot2;
    } else if (this.location_text == 'Motor') {
      this.actionsTitle = "Address this Issue";
      this.actions = this.actionsMotors;
    } else if (this.location_text == 'Gearbox'){
      this.actionsTitle = "Address this Issue";
      this.actions = this.actionsGearboxes;
    } else
    {
      this.actionsTitle = "Address this Issue";
      this.actions = this.actionsAddress;
    }
  }
  toggleModal() {
    this.showModal = !this.showModal;
  }
  toggleIssueModal(item) {
    this.actionItem = item;
    this.showIssueModal = !this.showIssueModal;

  }
  toggleNoteModal() {
    this.showNoteModal = !this.showNoteModal;
  }
  toggleNotificationModal() {
    if (!this.showNotificationModal) {
      this.teamData.forEach(function (t) {
        if (t.checked) {
          this.showModal = true;
          this.showNotificationModal = true;
        }
      }.bind(this))
    }
    else {
      this.showNotificationModal = !this.showNotificationModal;
    }
  }

  checked(flag, i) {
    if (flag) {

    }
  }

  public onIssueDialogClose(e:any){
    if(e.item.type=='QF'){
      this.quickFix={issueResolved:e.issueResolved};
    }else if(e.item.type=='TOP'){
      this.topFix={issueResolved:e.issueResolved};
    }else if(e.item.type=='CHILD'){
      this.actions[e.item.index].issueResolved=e.issueResolved?1:2;
      console.log(this.actions[e.item.index]);
    }
  }

  getCircleIcon(flag) {
    if (flag)
      return "./assets/images/checked-circle.svg"
    else
      return "./assets/images/unchecked-circle.svg"
  }

}
