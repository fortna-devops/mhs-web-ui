import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NouisliderModule } from 'ng2-nouislider';
import { NvD3Module } from 'ng2-nvd3';
import { AlertDetailPageComponent } from './alert-detail-page/alert-detail-page.component';
import { DetailPageHeaderComponent } from './detail-page-header/detail-page-header.component';
import { DetailInsightsComponent } from './detail-insights/detail-insights.component';
import { DetailActionsComponent } from './detail-actions/detail-actions.component';
import { FixDialogComponent } from './fix-dialog/fix-dialog.component';
import { ActionStepsComponent } from './action-steps/action-steps.component';
import { SharedModule } from '../shared/shared.module';
import { DetailInsightsService } from './detail-insights/detail-insights.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NouisliderModule,
    NvD3Module,
    SharedModule
  ],
  declarations: [
    AlertDetailPageComponent,
    DetailPageHeaderComponent,
    DetailInsightsComponent,
    DetailActionsComponent,
    FixDialogComponent,
    ActionStepsComponent
  ],
  providers: [
    DetailInsightsService
  ],
  exports: []
})
export class AlertDetailsModule { }
