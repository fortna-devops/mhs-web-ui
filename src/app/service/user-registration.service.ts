import { Inject, Injectable } from '@angular/core';
import { CognitoCallback, CognitoUtil } from './cognito.service';
import { AuthenticationDetails, CognitoUser, CognitoUserAttribute } from 'amazon-cognito-identity-js';
import { NewPasswordUser } from '../public/auth/newpassword/newpassword.component';
import { AuthService } from '../core/auth.service';
import * as CognitoIdentityServiceProvider from 'aws-sdk/clients/cognitoidentityserviceprovider';
import * as AWS from 'aws-sdk/global';

@Injectable()
export class UserRegistrationService {

  constructor(
    private authService: AuthService,
    @Inject(CognitoUtil) public cognitoUtil: CognitoUtil
  ) { }

  register(user: any, callback: CognitoCallback) {
    const params = {
      UserPoolId: 'us-east-1_eEombidgG',
      Username: user.name,
      DesiredDeliveryMediums: ['EMAIL'],
      ForceAliasCreation: true || false,
      TemporaryPassword: user.password,
      UserAttributes: [
        {
          Name: 'nickname',
          Value: user.name
        },
        {
          Name: 'email',
          Value: user.email
        },
        {
          Name: 'phone_number',
          Value: user.phone_number
        },
        {
          Name: 'custom:subscription',
          Value: JSON.stringify(user.subscription)
        }
      ]
    };

    AWS.config.update({
      accessKeyId: this.authService.getAccessKey(),
      secretAccessKey: this.authService.getSecretAccessKey()
    });

    const client = new CognitoIdentityServiceProvider({apiVersion: '2016-04-19', region: 'us-east-1'});

    return new Promise((resolve, reject) => {
      client.adminCreateUser(params, function(err, data) {
        if (err) {
          resolve(err);
        } else {
          resolve(data);
        }
      });
    });
  }

  addUserToGroup(user, userGroup) {
    // TODO: get pool id
    const params = {
      GroupName: userGroup,
      UserPoolId: 'us-east-1_eEombidgG',
      Username: user['Username']
    };
    AWS.config.update({
      accessKeyId: this.authService.getAccessKey(),
      secretAccessKey: this.authService.getSecretAccessKey()
    });

    const cognitoIdentityServiceProvider = new CognitoIdentityServiceProvider({apiVersion: '2016-04-19', region: 'us-east-1'});

    return new Promise((resolve, reject) => {
      cognitoIdentityServiceProvider.adminAddUserToGroup(params, function(err, data) {
        if (err) {
          resolve(err);
        } else {
          resolve(data);
        }
      });
    });
  }

  confirmRegistration(username: string, confirmationCode: string, callback: CognitoCallback): void {

    let userData = {
      Username: username,
      Pool: this.cognitoUtil.getUserPool()
    };

    let cognitoUser = new CognitoUser(userData);

    cognitoUser.confirmRegistration(confirmationCode, true, function (err, result) {
      if (err) {
        callback.cognitoCallback(err.message, null);
      } else {
        callback.cognitoCallback(null, result);
      }
    });
  }

  resendCode(username: string, callback: CognitoCallback): void {
    let userData = {
      Username: username,
      Pool: this.cognitoUtil.getUserPool()
    };

    let cognitoUser = new CognitoUser(userData);

    cognitoUser.resendConfirmationCode(function (err, result) {
      if (err) {
        callback.cognitoCallback(err.message, null);
      } else {
        callback.cognitoCallback(null, result);
      }
    });
  }

  newPassword(newPasswordUser: NewPasswordUser, callback: CognitoCallback): void {
    console.log(newPasswordUser);
    // Get these details and call
    //cognitoUser.completeNewPasswordChallenge(newPassword, userAttributes, this);
    let authenticationData = {
      Username: newPasswordUser.username,
      Password: newPasswordUser.existingPassword,
    };
    let authenticationDetails = new AuthenticationDetails(authenticationData);

    let userData = {
      Username: newPasswordUser.username,
      Pool: this.cognitoUtil.getUserPool()
    };

    console.log("UserLoginService: Params set...Authenticating the user");
    let cognitoUser = new CognitoUser(userData);
    console.log("UserLoginService: config is " + AWS.config);
    cognitoUser.authenticateUser(authenticationDetails, {
      newPasswordRequired: function (userAttributes, requiredAttributes) {
        // User was signed up by an admin and must provide new
        // password and required attributes, if any, to complete
        // authentication.

        // the api doesn't accept this field back
        delete userAttributes.email_verified;
        cognitoUser.completeNewPasswordChallenge(newPasswordUser.password, requiredAttributes, {
          onSuccess: function (result) {
            callback.cognitoCallback(null, userAttributes);
          },
          onFailure: function (err) {
            callback.cognitoCallback(err, null);
          }
        });
      },
      onSuccess: function (result) {
        callback.cognitoCallback(null, result);
      },
      onFailure: function (err) {
        callback.cognitoCallback(err, null);
      }
    });
  }
}
