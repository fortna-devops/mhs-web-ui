import { Component, OnInit } from '@angular/core';
import { TopNavService } from '../shared/top-nav/top-nav.service';


@Component({
  selector: 'app-facility-overview',
  templateUrl: './facility-overview.component.html',
  styleUrls: ['./facility-overview.component.scss']
})
export class FacilityOverviewComponent implements OnInit {

  constructor(private topNav: TopNavService) { }

  ngOnInit() {
    this.topNav.hide();
    this.topNav.setTitle('Facility Health Overview');
  }
}
