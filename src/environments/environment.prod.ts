export const environment = {
  production: true,

  region: 'us-east-1',

  apiURL: 'https://fvflqmj6ab.execute-api.us-east-1.amazonaws.com/prod',

  identityPoolId: '',//'us-east-1:fbe0340f-9ffc-4449-a935-bb6a6661fd53',
  userPoolId: 'us-east-1_eEombidgG',
  clientId: '5duuv4418bja5en9dd39kdk6ms',

  rekognitionBucket: 'rekognition-pics',
  albumName: "usercontent",
  bucketRegion: 'us-east-1',

  ddbTableName: 'LoginTrail',

  cognito_idp_endpoint: '',
  cognito_identity_endpoint: '',
  sts_endpoint: '',
  dynamodb_endpoint: '',
  s3_endpoint: ''
};
